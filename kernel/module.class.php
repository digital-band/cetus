<?php 

class Module {
	protected $classname;
	protected $modulename;
	protected $params;
	protected $tables;

	protected $mainObj;
	protected $dbObj;
	protected $cache;	

	protected function __construct(Main $mainObj = null){
		$this->setParams();
		if ($mainObj) {
			$this->setMain($mainObj);
			$this->setCacher($mainObj->getCacher());
			
			// we need new instance of DB object for each module but one connection object instance for all of them
			$this->dbObj = $mainObj->load("db");
			$this->dbObj->setConnectionObject($mainObj->getConnectionObject());
			$this->dbObj->setInstanceOwner($this->classname);
			
			if ($this->tables) $this->dbObj->setTables($this->tables);
		}
		
	}
	
	// getters {
	public function getModulename() {
		return $this->modulename;
	}
	
	public function getClassname() {
		return $this->classname;
	}
	
	public function getParams() {
		return ($this->params) ? $this->params : null;
	}
	// getters }
	
	// setters {
	public function setParams(array $params = null) {
		if (!empty($params)) {
			$this->params = $params;
		} else {
			$config = MODULE_DIR.$this->classname."/config.php";
			if (file_exists($config)) {
				require $config;
				if (!empty($params)) $this->params = $params;
			}
		}
	}
	
	public function setMain(Main $mainObj = null) {
		$this->mainObj = $mainObj;
	}
	
	public function setDB(DB $dbObj = null) {
		$this->dbObj = $dbObj;
	}
	
	public function setCacher($cache = null) {
		$this->cache = $cache;
	}
	
	public function getCacher() {
		return $this->cache;
	}
	// setters }
}

?>