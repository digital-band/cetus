<?php 

class Connection {
	private static $_instance;
	private $connection = null;
	
	private function __construct(){

	}
	
	public function __destruct() {
		$this->disconnect();
	}
	
	private function __clone(){
		
	}
	
	public static function getInstance() {
		if (null === self::$_instance) {
			return self::$_instance = new self();
		} else return self::$_instance;
	}
	
	public function getConnection(){
		if (!$this->connection) $this->connection = $this->connect();
		
		return $this->connection;
	}
	
	public function setConnection($connection){
		$this->connection = $connection;
	}
	
	private function connect(){
		if (!$this->connection) {
			$connection = new mysqli(
					DB_SERVER,
					DB_USER,
					DB_PASS,
					DB_NAME
			);
				
			// Check connection
			if (mysqli_connect_errno($connection)) {
				$GLOBALS['log'][] = "Failed to connect to Database! Look at log file. <br>";
				Main::log(mysqli_connect_error());
				//$connection = null;
			} else {
				$connection->set_charset(DB_CHARSET);
				$this->connection = $connection;
				$GLOBALS['log'][] = "<query>Connected</query>";
			}
			$GLOBALS['db_active'] = 1;
	
		} else $connection = $this->connection;
			
		return $connection;
	}
	
	public function disconnect(){
		if ($this->connection) {
			$this->connection->close();
			//Main::log("DB disconnected.");
		}
			
	}
}


?>