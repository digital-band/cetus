<?php 
/*
 * implements database operations
 */
class DB {
	private $connection = null;
	private $tables = Array();
	private $instance_owner = null;
	private $connObj = null;
	
	public function __construct(Connection $connObj = null){
		if ($connObj) {
			$this->connObj = $connObj;
		}
	}
	
	// getters {
	public function getInstanceOwner(){
		return $this->instance_owner;
	}
	
	public function getConnection(){
		return $this->connection;
	}
	
	public function getTables(){
		return $this->tables;
	}
	
	// getters }
	
	// setters {
	public function setInstanceOwner($classname){
		$this->instance_owner = $classname;
	}
	
	public function setConnection($connection){
		$this->connection = $connection;
	}
	
	public function setConnectionObject(Connection $connObj){
		$this->connObj = $connObj;
	}
	
	public function setTables($tables){
		// sets fields for a tables
		$this->tables = ($tables) ? $tables : null;
	
		if ($this->tables) return true;
	}
	// setters }
	
	private function connect(){
		if (!$this->connection) {
			if (!$this->connObj) {
				require_once 'connection.class.php';
				$this->connObj = Connection::getInstance();
			}
			
			$connection = $this->connection = $this->connObj->getConnection();
			
		} else $connection = $this->connection;
		
		return $connection;
	}
	
	private function disconnect(){
		if ($this->connection) {
			$this->connection->close();
			Main::log("DB disconnected.");
		}
		
	}

	public static function fetch($sql_result) {
		if ($sql_result) return $sql_result->fetch_assoc();
	}
	
	public static function fetchAll($sql_result, $numeration = "id"){
		$list = Array();
		if (!empty($sql_result)){
			while ($row = $sql_result->fetch_assoc()){
				if ($numeration && array_key_exists($numeration, $row)) {
					$list[$row[$numeration]] = $row;
				} else $list[] = $row;
			}
		}
		return $list;
	}

	final function delete($table, $query){
		$this->connect();
	
		if (!empty($this->tables[$table])) $table = $this->tables[$table]['table'];
	
		$query = "DELETE from ".DB_PREFIX.$table." WHERE ".$query;
		$result = $this->connection->query($this->safeQ($query));
	
		return $result;
	}

	public function escape($string, $clear = false){
		// clear = true means delete all chars listed below
		$pattern = "#[\'\"\`\n\r\,\;\:\t[:space:]]#";
	
		if ($clear) {
			$result = preg_replace($pattern, "", $string);
		} else {
			$result = preg_replace_callback(
					$pattern,
					function ($matches) {
						return "\\".$matches[0];
					},
					$string
			);
		}
	
		return $result;
	}
	// ------------------
	final function ext_Select($table, $query) {
		$this->connect();
	
		if ($stmt = $this->connection->prepare("SELECT * FROM $table WHERE name=?")) {
	
			// Bind a variable to the parameter as a string.
			$stmt->bind_param("s", $name);
	
			// Execute the statement.
			$stmt->execute();
	
			// Get the variables from the query.
			$stmt->bind_result($pass);
	
			// Fetch the data.
			$stmt->fetch();
	
			// Display the data.
			printf("Password for user %s is %s\n", $name, $pass);
	
			// Close the prepared statement.
			$stmt->close();
	
		}
	}
	// ------------ http://www.wikihow.com/Prevent-SQL-Injection-in-PHP
	private function formatField($value, $format = Array('type' => '', 'format' => 'default')) {
		// preapare field before inserting to DB
		if (!empty($format['type'])) {
			// specific formatting
			switch ($format['type']) {
				case "int":
					$value = (is_numeric($value)) ? $value : '';
			}
			$result = $value;
		} else
			$result = $value;
	
		return $result;
	}

	final function getList($table, $query = null){
		// depricated
		$this->connect();
		if (!empty($this->tables[$table])) $table = $this->tables[$table]['table'];
		$list = Array();
	
		$query = "SELECT * from ".DB_PREFIX.$table.(($query !== null) ? " WHERE ".$query :"")." ORDER BY id";
		echo $query;
		$result = $this->connection->query($this->safeQ($query));
	
		while ($row = $result->fetch_assoc()){
			$list[] = $row;
		}
		return $list;
	}

	final function insert($table, $entry){
		$new_id = null;
			
		if (empty($this->tables) || array_key_exists($table, $this->tables)) {
			$this->connect();
	
			$keys = Array();
			$values = Array();
	
			if (empty($this->tables)) {
				// uncontrolled insert
				foreach ($entry as $k => $value){
					$keys[] = "`".$this->safeQ($k)."`";
					$values[] = "'".$this->safeQ($value)."'";
				}
			} else {
				// controlled insert
				$table_alias = $table;
				$table = $this->tables[$table_alias]['table'];
					
				foreach ($entry as $k => $value){
					// add field if it exists in template
					if (array_key_exists($k, $this->tables[$table_alias]['fields'])) {
						$value = $this->formatField($value, $this->tables[$table_alias]['fields'][$k]);
							
						$keys[] = "`".$this->safeQ($k)."`";
						$values[] = "'".$this->safeQ($value)."'";
					}
	
				}
			}
			if (!empty($keys)) {
				$keys = implode(", ", $keys);
				$values = implode(", ", $values);
					
				$query = "INSERT INTO ".DB_PREFIX.$table. " (".$keys.") VALUES (".$values.")";
	
				$result = $this->connection->query($query);
				$new_id = $this->connection->insert_id;
			}
		}
			
		return $new_id;
	}

	final function query($query){
		$this->connect();
		$result = $this->connection->query($query);
	
		if ($this->connection->error) {
			Main::log("Query Errormessage: %s\n ".$this->connection->error);
			$GLOBALS['log'][] = $this->connection->error;
		}
			
		return $result;
	}

	public final function restoreQ($entry){
		if (!empty($entry)){
			foreach ($entry as $k => $v){
				if (is_array($v)) {
					$entry[$k] = $this->restoreQ($v);
				} else $entry[$k] = stripslashes($v);
			}
		}
	
		return $entry;
	}

	public final function safeQ($query){
		$this->connect();
	
		$result = $this->connection->real_escape_string($query);
	
		return $result;
	}
	
	final public function select($table, $query, $order = null, $direction = null){
		$this->connect();
		$result = null;
	
		if (!empty($this->tables[$table])) $table = $this->tables[$table]['table'];
	
		$query = "SELECT * from ".DB_PREFIX.$table.(($query !== null) ? " WHERE ".$query :"").
		" ORDER BY ".(!empty($order) ? $order : "`id`").(($direction) ? " ASC " : " DESC ");
		$result = $this->connection->query($query);
	
		$GLOBALS['log'][] = "<query>Query: </query>".$query;
	
		//Main::log( "Query: ".$query);
	
		return $result;
	}

	final function update($table, $entry, $query){
		$result = null;
	
		if (empty($this->tables) || array_key_exists($table, $this->tables)) {
			$this->connect();
			$values = Array();
				
			if (empty($this->tables)) {
				// uncontrolled update
				foreach ($entry as $k => $value){
					$values[] = "`".$this->safeQ($k)."` = '".$this->safeQ($value)."'";
				}
			} else {
				// controlled update
				$table_alias = $table;
				$table = $this->tables[$table_alias]['table'];
	
				foreach ($entry as $k => $value){
					// add field if it exists in template
					if (array_key_exists($k, $this->tables[$table_alias]['fields'])) {
						$value = $this->formatField($value, $this->tables[$table_alias]['fields'][$k]);
						$values[] = "`".$this->safeQ($k)."` = '".$this->safeQ($value)."'";
					}
				}
			}
			$values = implode(", ", $values);
	
			$full_query = "UPDATE ".DB_PREFIX.$table. " SET ".$values." WHERE ".$query;
			$result = $this->connection->query($full_query);
			$GLOBALS['log'][] = "<query>Query: </query>".$full_query;
		}
	
		return $result;
	}
	public final function validateInput($string){
		// clean out dangerous chars and return safe string or null
		$clean_string = $this->escape($string);
		if ($clean_string === $string) {
			$result = $string;
		} else $result = null;

		return $result;
	}


}

?>