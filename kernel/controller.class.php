<?php
/* This class implements common methods for models' controllers
 * 
 * ver 2
 */ 


class Controller {
	// child object, instance of controller 
	private $controllerObj = null;
	// requested action
	private $action = null;
	// user access level
	private $u_level = null;
	// module parameters (config.php)
	private $params;
	private $uri;
	private $childClassname = null;
	
	// module model object
	protected $obj;
	protected $mainObj;
	// controller access (security) levels
	protected $sec_levels = Array();
	// controller page template
	protected $template = null;
	// requested in controller id
	protected $id = null;
	
	protected function __construct(Main $mainObj, $moduleObj = null){
		$this->obj = $moduleObj;
		$this->mainObj = $mainObj;
		$this->uri = $this->mainObj->getUri();
		if ($this->obj) $this->params = $this->obj->getParams();
	
		$this->run();
	}
	
	// getters {
	public function getSecLevels(){
		return $this->sec_levels;
	}
	// getters }
	
	// setters {
	public function setParams($params = null){
		$this->params = $this->obj->getParams($params);
	}
	// setters }
	
	private function run(){
		set_error_handler(array($this, "ActionNotFoundException"));
		
		$this->childClassname = strtolower(substr(get_class($this), 0, -10));
		// detect action
		$this->action = ($this->mainObj->getUriPart(1) && $this->mainObj->getUriPart(0) == $this->childClassname) ? $this->mainObj->getUriPart(1) : "main";
		
		// id can be numeric or string
		if (!$this->mainObj->getUriPart(2)) $this->id = $this->mainObj->getUriPart(2);
		
		$this->orderTable();
	
		if (method_exists($this, "action_".$this->action)) {
			$this->controllerObj = $this;
		} else {
			// action not found
			if (!empty($this->params['redirect_action'])) {
				// send all unexisting actions to specified one
				$this->action = $this->params['redirect_action'];
				$this->controllerObj = $this;
			} else {
				$this->mainObj->setPage(Main::error(404));
				$GLOBALS['log'][] = "Invalid action requested: ".$this->obj->classname." -> ".$this->action;
			}
		}
		
		// check user access level
		if (!empty($_SESSION['user']['level'])) {
			$this->u_level = $_SESSION['user']['level'];
			//$GLOBALS['log'][] = "Your access level: ".$this->u_level;
		}
		
		if ($this->controllerObj) {
			$a_level = null;
			// get access level for the action
			if (!empty($this->sec_levels[$this->action])) $a_level = $this->sec_levels[$this->action];
				
			//$GLOBALS['log'][] = "Action access level: ".$a_level;
				
			if ($a_level == null || Main::authorize($a_level)) {
				// if access is unlimited or user has the permission
				try {
					call_user_func(array($this->controllerObj, "action_".$this->action));
										
				} catch (ErrorException $e) {
					$this->mainObj->setPage(Main::error(404));
					$GLOBALS['log'][] = $e;
					Main::log($e);	
				}
			} else {
				Main::log("Permission denied for : ".$this->uri);
				if (empty($this->u_level)) {
					// not authorized user
					// remember requested link
					$_SESSION['user_redirect_link'] = preg_replace("#^\/#", "", $this->uri);
					Main::redirect("users/login/");
				} else {
					// authorized but without permission
					if (!empty($_SESSION['user_redirect_link'])) {
						// if user has been redirected after loggin in
						unset($_SESSION['user_redirect_link']);
						Main::redirect("/");
					} else $this->mainObj->setPage(Main::error(404));
					
					$GLOBALS['log'][] = "Permission denied. Min level: ".$a_level;
				}
			}
		}

		// load access levels
		if (!empty($this->u_level) && !empty($this->sec_levels)) {
			$locked = Array();
			foreach ($this->sec_levels as $action => $level) {
				$locked[$action] = ($this->u_level <= $level) ? 0 : 1;
			}
			$this->mainObj->append('locked', $locked);
		}
		
		// set template and vars
		if (!empty($this->sec_levels)) $this->mainObj->assign('a_levels', $this->sec_levels);
		if (!$this->mainObj->getPageProperty('template')) $this->mainObj->setPageProperty('template', $this->template);
		if (!empty($this->obj->classname)) $this->mainObj->assign('module', $this->obj->classname);
		
		// debug info
		$GLOBALS['log'][] = "-------------------------------------";
		$GLOBALS['log'][] = "Controller: ".$this->childClassname;
		$GLOBALS['log'][] = "Action: ".$this->action;
		if ($this->id) $GLOBALS['log'][] = "ID: ".$this->id;
		$GLOBALS['log'][] = "-------------------------------------";

	}
	
	private function actionNotFoundException($errno, $errstr, $errfile, $errline, array $errcontext) {
		if (0 === error_reporting()) {
			return false;
		}
		throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
	}
	
	protected function orderTable() {
		// table operations
		$order = 0;
		$direction = 0;
		
		if (!empty($_GET['order']))		$order = $_GET['order'];
		if (!empty($_GET['direction']))	$direction = $_GET['direction'];
		
		$this->order = $order;
		$this->direction = $direction;
		$direction = ($this->direction == 0) ? 1 : 0;
		$this->mainObj->assign('direction', $direction);
	}
	
}


?>