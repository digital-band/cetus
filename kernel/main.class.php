<?php 

class Main{
	private static $_instance;
	
	private $classname;
	private $modules = Array();
	private $uri;  
	private $uri_parts = Array();
	private $modules_ext = null;
	private $default_module = "content";
	private $page = Array();
	private $tpl;
	private $page_properties;
	
	// database operations object
	private $dbObj;
	// connection object
	private $connObj;
	// cache engine object
	private $cacheObj = null;
	

	private function __construct() {
		$this->classname = get_class($this);
		$this->dbObj = $this->load('db');		
		$this->connObj = $this->load('connection', null, false);

		$this->dbObj->setInstanceOwner($this->classname);		
	 	$this->modules = $this->modulesList(MODULE_DIR);
		$this->uri_parts = $this->get_uri_parts();
		$this->uri = $this->get_uri();
		// run cache engine
		$this->cacheObj = $this->loadCacher();

	}
	
	public static function getInstance() {
		if (null === self::$_instance) {
			return self::$_instance = new self();
		} else return self::$_instance;
	}
	
	private function __clone(){
	}
	
	//  getters {
	public function getDB() {
		return $this->dbObj;
	}
	
	public function getCacher() {
		return $this->cacheObj;
	}
	
	public function getConnectionObject() {
		return $this->connObj;
	}
	
	public function getClassname() {
		return $this->classname;
	}
	
	public function getPage() {
		return $this->page;
	}

	public function getPageProperty($key) {
		if (!empty($this->page[$key])) return $this->page[$key];
	}
	
	public function getPageProperties(){
		return (!empty($this->page_properties)) ? $this->page_properties : null;
	}
	
	public function getModules() {
		return ($this->modules) ? $this->modules : $this->modulesList(MODULE_DIR);
	}
	
	public function getModulesExt($level = null) {
		return ($this->modules_ext) ? $this->modules_ext : $this->modulesListExt($level);
	}
	
	public function getUri() {
		return ($this->uri) ? $this->uri : $this->get_uri();
	}
	
	public function getUriParts($number = null) {
		if (is_null($number)) {
			// return full list of uri parts
			$result =  ($this->uri_parts) ? $this->uri_parts : $this->get_uri_parts();
		} else {
			if (empty($this->uri_parts[$number])) {
				$this->uri_parts = $this->get_uri_parts();
				$result = (!empty($this->uri_parts[$number])) ? $this->uri_parts[$number] : null;
			} else $result = $this->uri_parts[$number];
		}
		
		return $result;
	}
	
	public function getUriPart($number) {
		$result = (!empty($this->uri_parts[$number])) ? $this->uri_parts[$number] : null;
		
		return $result;
	}
	
	public function getTpl() {
		return $this->tpl;
	}

	// getters }
	
	//  setters {

	public function setTpl(TemplateEngine $tpl){
		$this->tpl = $tpl;
	}
	
	public function setPage($page = Array()) {
		$error = Array();
		if (!is_array($page)) {
			$error[] = "Invalid page: is not array";
		}
		if (empty($page['title'])) {
			$error[] = "Invalid page: title is empty";
		}
		
		$this->page = $page;
		
		if (empty($error)) {
			return true;
		} else {
			$GLOBALS['log'][] = $error;
			return $error;
		}
	}
	
	public function setPageProperty($key, $value) {
		$this->page[$key] = $value;
	}
	
	public function setPageProperties($value = Array()){
		$this->page_properties = $value;
	}
	
	public function assign($key, $value = null) {
		// set var to tpl object
		if (is_array($key)) {
			$this->tpl->assign($key);
		} else $this->tpl->assign($key, $value);
	}
	
	public function append($key, $value) {
		// append var to tpl object
		$this->tpl->append($key, $value, true);
	}
	
	
	// setters } 

	public static function authorize($level_limit){
		// check user's access level
		$result = false;
		$user = Array();
	
		if (!empty($_SESSION['user']['id'])) {
			$user = $_SESSION['user'];
			if (!empty($user['level'])) $level = $user['level'];
	
			if (!empty($level)) {
				if ($level <= $level_limit) {
					$result = $user;
				}
			}
		}
			
		return $result;
	}

	public static function error($type, $uri = null){
		if ($uri == null) $uri = Main::get_uri();
		$page = Array();
		$page['template'] = "error";
		switch ($type){
			case "404":
				$page['title'] = $page['text'] = "404 page not found";
				header("Location: /".$uri, true, 404);
	
				break;
			case "403":
				$page['title'] = $page['text'] = "403 forbidden";
				header("Location: /".$uri, true, 403);
					
				break;
		}
	
		return $page;
	}
	
	public static function get_uri_parts(){
		$result = Array();
		// returns uri parts as array
		$uri = @$_SERVER['REQUEST_URI'];
		// remove GET params
		$uri = explode('?', $uri);
		$uri = $uri[0];
	
		$uri_parts = array();
			
		$uri_parts = array_filter(explode('/', $uri));
			
		foreach ($uri_parts as $k => $v){
			// clean from GET params
			if (substr($v, 0, 1) == "?") unset($uri_parts[$k]);
		}
	
		foreach ($uri_parts as $k => $v) {
			$result[] = $v;
		}
	
		return $result;
	}
	
	public static function get_uri(){
		// returns uri as string
		$uri = @$_SERVER['REQUEST_URI'];
		// remove GET params
		$uri = explode('?', $uri);
		$uri = $uri[0];
	
		return $uri;
	}

	public static function highlight($string){
		$result = "";
		// vars
		$matches = Array();
		$pattern = "#\"[^\"]*\"#";
		preg_match_all($pattern, $string, $matches);
	
		foreach ($matches as $k => $v){
			echo $v."<br>";
		}
	
		return $result;
	}
	
	public static function http_send($uri, $method = "GET", $params, $timeout = 5){
		// sends request to $uri and returns response
		$result = null;
		$params_line = "";
	
		if ($method == "GET" && !empty($params)) {
			$params_line = "?";
			$it = 0;
			foreach ($params as $key => $value) {
				$params_line .= (($it > 0) ? "&" : "")."$key=$value";
				$it++;
			}
				
			$uri .= $params_line;
		}
		$link = curi_init();
		curi_setopt($link, CURLOPT_URL, $uri); // set uri to post to
		curi_setopt($link, CURLOPT_HEADER, false);
		curi_setopt($link, CURLOPT_NOBODY, false);
		curi_setopt($link, CURLOPT_FAILONERROR, 1);
		curi_setopt($link, CURLOPT_RETURNTRANSFER, 1); // return into a variable
		curi_setopt($link, CURLOPT_TIMEOUT, $timeout); // times out after %s
	
		if ($method == "POST" && !empty($params)) {
			$GLOBALS['log'][] = $method;
			$GLOBALS['log'][] = $params;
			curi_setopt($link, CURLOPT_POST, 1); // set method
			curi_setopt($link, CURLOPT_POSTFIELDS, $params); // add POST fields
		}
	
		$GLOBALS['log'][] = $uri;
	
	
		$result = curi_exec($link); // run the whole process
		curi_close($link);
	
		return $result;
	}
	
	public static function log($entry, $time = true){
		file_put_contents(LOG, (($time) ? date("Y-m-d H:i:s", time()).": " : '').$entry."\r",  FILE_APPEND | LOCK_EX);
	}

	public static function logout(){
		$user = $_SESSION['user'];
		Main::log($user['login']." logged out", true);
		unset($_SESSION['user']);
		unset($_SESSION['user_redirect_link']);
		unset($_SESSION['settings']);
	}
		
	public static function print_r($array){
		return "<pre>".print_r($array, true). "</pre>";
	}
	
	public static function print_debug(){
		// debug out as html
		if (defined("DEBUGGING")) {
			if (DEBUGGING) {
				$log = Array();
				$output = "";
				
				if (!empty($GLOBALS['debug_info'])) $log = array_merge($log, $GLOBALS['debug_info']);
				if (!empty($GLOBALS['log'])) $log = array_merge($log, $GLOBALS['log']);

				if (!empty($log)) {
					$output .= '<div class="debug_info">';
					foreach ($log as $line){
						if (is_array($line)) $line = Main::print_r($line); 
						$output .=  '<div class="line">'.$line.'</div>';
					}
					$output .=  '</div>';
				}
				echo $output;
			}
		}
	}
	
	public static function redirect($uri = ''){
		if (!$uri) {
			header("Location: ".$_SERVER['REQUEST_URI']);
		} else if($uri == "/") {
			header("Location: /");
		} else {
			header("Location: /".$uri);
		}
	}

	private function compare($a, $b){
		// comparator for Order method
		$order = ($this->order) ? $this->order : "id";
		$direction = ($this->direction) ? $this->direction : 0;
	
		if ($a[$order] == $b[$order]) {
			return 0;
		}
		if ($direction) {
			return ($a[$order] > $b[$order]) ? -1 : 1;
		} else {
			return ($a[$order] < $b[$order]) ? -1 : 1;
		}
	}
	
	public function fetch($template) {
		return $this->tpl->fetch($template);
	}

	public final function getRoutes($cur_uri){
		// get cp_uri from all modules to route right way if some module uses specific entry points
		$dir = opendir(MODULE_DIR) or die("Can't open the folder");
		$module_folder_name = $cur_uri;
	
		while ($item = readdir($dir)){
			if ($item != "." && $item != ".." && is_dir(MODULE_DIR.$item)) {
				$config = MODULE_DIR.$item."/config.php";
				if (file_exists($config)){
					require $config;
					if (!empty($params['cp_uri'])) {
						if (str_replace("/", "", $params['cp_uri']) == str_replace("/", "", $cur_uri) ) {
							$module_folder_name = $item;
						}
					}
				}
			}
		}
	
		return $module_folder_name;
	}
	
	public function load($classname, $args = Array(), $new = true){
		// load class by name or getInstance()
		$object = null;
	
		if (file_exists(KERNEL_C.$classname.".class.php")) {
			// if one of the kernel classes
			$file = KERNEL_C.$classname.".class.php";
		} else
		if (in_array($classname, $this->modulesList())) {
			// if it is a module
			$file = MODULE_DIR.$classname."/models/".$classname.".class.php";
		} else $file = null;
	
		if ($file != null) {
			require_once $file;
			if ($new) {
				// create new instance
				if (!empty($args)) {
					$object = new $classname($args);
				} else {
					$object = new $classname();
				}
			} else {
				$object = $classname::getInstance();
			}
		}
	
		return $object;
	}
	
	public function loadCacher() {
		// run cache engine
		require_once(ROOT.'/lib/phpfastcache/phpfastcache.php');
		return new phpFastCache();
	}

	public function login($entry){
		// login superuser
		if ( sha1($entry['pass']) === SUPERPASS && $entry['id'] == SUPERUSER) {
			$user = Array();
			$this->log("SUPERUSER logged in", true);
			$user['id'] = "000";
			$user['login'] = SUPERUSER;
			$user['name'] = SUPERUSER;
			$user['pass'] = sha1($entry['pass']);
			$user['level'] = 1;
			$user['category_info']['level'] = 1;
	
			$_SESSION['user'] = $user;
	
			return $user;
		} else {
			return null;
		}
	}
	
	public final function modulesList($path = MODULE_DIR){
		$arr = array();
		$dir = opendir($path) or die("Can't open the folder");
		while ($item = readdir($dir)){
			if ($item != "." && $item != ".." && is_dir($path.$item)) {				
				$arr[] = $item;
			}
		}
	
		return $arr;
	}	
	
	public final function modulesListExt($a_level = 0, $path = MODULE_DIR){
		$result = array();
		$dir = opendir($path) or die("Can't open the folder");
		
		while ($item = readdir($dir)){
			if ($item != "." && $item != ".." && is_dir($path.$item)) {
				$module = Array();
				$params = Array("enable" => true); // switch the module on by default
				$config = MODULE_DIR.$item."/config.php";
				
				if (file_exists($config)) {
					require $config;	

					if (!empty($params)) $module['name'] = $params['name'];
					$module['cp_uri'] = (!empty($params['cp_uri'])) ? $params['cp_uri'] : $item."/" ;
					if (!empty($entry_points)) $module['entry_points'] = $entry_points;
					if (!empty($admin_points)) $module['admin_points'] = $admin_points;
					if (!empty($params)) $module['params'] = $params;

				} else {
					$module['name'] = $item;
				}
				
	
				if ($params['enable'] != 0 ) {
					if (!empty($params['a_level'])) {
						if ($a_level <= $params['a_level']) $result[$item] = $module;
					} else $arr[$item] = $module;
				
				}
				
				$this->modules_list = $result;
			}
		}
	
		return $result;
	}
	
	public final function handlePublicURI($uri_parts = null){
		// public request
		// catch uri and load module else load default module or index
		// if a module has public controller load it otherwise - load default module controller
		if (!$uri_parts) $uri_parts = $this->uri_parts = $this->get_uri_parts();
		$uri = $uri_parts[0];
		$this->page = null;

		$modules = $this->modulesListExt();
		
		$controller = MODULE_DIR.$uri."/controllers/public/".$uri.".controller.php";
		$module = (key_exists($uri, $modules) && file_exists($controller)) ? $uri : $this->default_module ;
		
		$build_controller = KERNEL_C."controller.class.php";
		$build_module = KERNEL_C."module.class.php";
		
		require_once $build_controller;
		require_once $build_module;
		
		if (key_exists($module, $modules)) {
			//echo $module;
			// if a module requested load it
			$model = MODULE_DIR.$module."/models/".$module.".class.php";
			$controller = MODULE_DIR.$module."/controllers/public/".$module.".controller.php";
			$config = MODULE_DIR.$module."/config.php";
			$params = Array("enable" => true); // switch the module on by default

			if (file_exists($config)) {
				require $config;
			
				if (!empty($entry_points)) {
			
					$path = $uri_parts[0]."/";
					$longpath = '';
					foreach ($uri_parts as $i => $part){
						$longpath .= $part."/";
					}

					if (!empty($entry_points[$longpath])) {
						$controller = MODULE_DIR.$module."/controllers/public/".$entry_points[$longpath];
					} elseif (!empty($entry_points[$path])) $controller = MODULE_DIR.$module."/controllers/public/".$entry_points[$path];
				}
			}
					
			if ($params['enable'] != false) {
				if (file_exists($model)) {
					require $model;
					$obj = new $module($this /* includes Main, DB, cache*/);
				}
			
				if (file_exists($controller)) {
					require $controller;
					$controller_classname = ucfirst($module."Controller");
					$controllerObj = new $controller_classname($this, $obj);
				}
				
				if (!empty($this->page['template'])) {
					if ($this->page['template'] != "error") $this->page['template'] = MODULE_DIR.$module."/views/public/".$this->page['template'];
				} 
				
			} else $this->page = $this->error("404");	
			

		} else $this->page = $this->error("404");
		
		return $this->page;
	}
	
	public final function handlePrivateURI($uri_parts = null){
		if (!$uri_parts) $uri_parts = $this->uri_parts = $this->get_uri_parts();
		$uri = $uri_parts[1];
		$this->page = null;

		// try to load common admin actions {
		$module_class = KERNEL_C."module.class.php";
		$cp_class = ROOT."/admin/models/cp.class.php";
		
		$model = ROOT."/admin/models/control.class.php";	
		$controller = ROOT."/admin/controllers/maincp.controller.php";	
	
		require_once $module_class;
		require_once $cp_class;
		
		require_once $model;
		require_once $controller;
		
		$controlObj = new Control();
		$maincpObj = new MainCP($this, $controlObj);
		// }
		
		/* detect custom redirect for modules
		 * can be optimized or disabled to reduce system load, use if you have specified (not equal to module name) uris for modules
		* $uri = $this->getRoutes($uri);
		*
		*/
		
		// Load module
		if (in_array($uri, $this->modules)) {
			// if a module requested load it
			$model = MODULE_DIR.$uri."/models/".$uri.".class.php";
			$controller = MODULE_DIR.$uri."/controllers/admin/".$uri."cp.controller.php";
			$config = MODULE_DIR.$uri."/config.php";
				
			$params = Array("enable" => true); // switch the module on by default
	
			if (file_exists($config)) {
				require $config;
			
				if (!empty($admin_points)) {
					$path = $uri_parts[2]."/";
					$longpath = '';
					foreach ($uri_parts as $i => $part){
						if ($i > 1) $longpath .= $part."/";
					}
			
					if (!empty($admin_points[$longpath])) {
						$controller = MODULE_DIR.$uri."/controllers/admin/".$admin_points[$longpath];
					} elseif (!empty($entry_points[$path])) $controller = MODULE_DIR.$uri."/".$admin_points[$path];
				}
			}
			
			if (file_exists($model)) {
				require_once $model;
				$obj = new $uri($this/* includes Main, DB, cache*/);
			}

			if (file_exists($controller) && $params['enable'] == true) {
				require_once $controller;
				$controller_classname = $uri."CP";
				$cpObj = new $controller_classname($this, $obj);
	
				if (empty($this->page['template'])) {
					$this->page['template'] = MODULE_DIR.$uri."/views/admin/main";
				} else if ($this->page['template'] != "error") 
					$this->page['template'] = MODULE_DIR.$uri."/views/admin/".$this->page['template'];
	
			} else $this->page = $this->error("404");
	
		} else {
			if (!empty($this->page['template'])) {
				$this->page['template'] = $this->page['template'];
			} else $this->page['template'] = "index";
		}
		
		return $this->page;
	}
	
	public function reKey($array, $key){
		// Update key of array		
		$iter = 0;
		$result = Array();
		if (!empty($array)) {
			foreach ($array as $v){
				$key_set = ($key == "num") ? $iter : $v[$key];
				$result[$key_set] = $v;
				
				$iter++;
			}
			
		}
		
		return $result;
	}

	public function order($array, $order, $direction){
		// ordering array
		$result = Array();
		$this->order = $order;
		$this->direction = $direction;
		uasort($array, array("self", "compare"));
		$result = $array;
		
		return $result;
	}

	public function uniqueByPriority($array, $key, $priority){
		// make unique array by $key with $priority
		$result = Array();
		if (!empty($array)) {
			foreach ($array as $v){
				// check priority
				if (!key_exists($v[$key], $result) || strlen($v[$priority]) > strlen($result[$v[$key]][$priority])) $result[$v[$key]] = $v;
			}
		}
		$result = $this->reKey($result, "num");
	
		return $result;
	}
	
	
}


?>