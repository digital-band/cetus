<?php 

class Files {
	private $source_path = null;
	private $dest_path = null;
	
	//  getters {
	public function getSourcePath() {
		return $this->source_path;
	}
	
	public function getDestPath() {
		return $this->dest_path;
	}
	
	public function setSourcePath($path) {
		$this->source_path = $path;
	}
	
	public function setDestPath($path) {
		$this->dest_path = $path;
	}
	
	
	public function uploadFile($file, $type, $file_size, $destination = "", $image_params = Array('width' => null, 'height' => null, 'quality' => null, 'crop' => false), $file_format = null){
		// validate uploaded file
		if (empty($image_params['width'])) $image_params['width'] = null;
		if (empty($image_params['height'])) $image_params['height'] = null;
		if (empty($image_params['quality'])) $image_params['quality'] = null;
		if (empty($image_params['crop'])) $image_params['crop'] = null;
		
		$GLOBALS['log'][] = $image_params;
		
		$result = $file;
		$result['error'] = Array();
	
		// the actual validation
		if (!is_uploaded_file($file['tmp_name'])) {
			$result['error'][] = "Couldn't upload file";
		} else
		if ($file['size'] > $file_size) {
			$result['error'][] = "File is too big";
		} else {
			// depricated method but I haven't PECL
			$mime_type = explode("/", mime_content_type($file['tmp_name']));
			$real_type = $mime_type[0];
			$real_format = $mime_type[1];
				
			$extention = explode(".", $file['name']);
			$extention = (!empty($extention[1])) ? $extention[1] : $real_format;
				
			if ($type != $real_type) {
				$result['error'][] = "Wrong file type";
			} else
				
			if (!($file_format == null || $file_format == $real_format)) {
				$result['error'][] = "Wrong file format";
			}
		}
	
	
		// if everything is allright
		if (empty($result['error'])) {
			// save the file
			if (!$destination) {
				$full_destination = UPLOAD_DIR;
			} else {
				$full_destination = UPLOAD_DIR.$destination;
			}
			$new_name = md5($file['tmp_name'].time()).".".$extention;
			$full_path = $full_destination.$new_name;

			$GLOBALS['log'][] = "Full path: ".$full_path;
			// create directory
			if (!file_exists($full_destination)) {
				try {
					$GLOBALS['log'][] = "Creating directory";
					mkdir($full_destination, 0777, true);
				} catch (Exception $e){
					Main::log($e);
				}
			}
			$GLOBALS['log'][] = "Full dest: ".$full_destination;
				
			// if it's valid uploded file
			if (move_uploaded_file($file['tmp_name'], $full_path)) {
				$saved_file = $full_path;
				// specific validation
				switch ($type) {
					case "image":
						// validate image size
						if ($image_params['width'] || $image_params['height']) {
							$i_size = null;
							try {
								$i_size = getImageSize($saved_file);
	
								// if image is too big we need to resize and compress it
								if ($i_size[0] > $image_params['width'] || $i_size[1] > $image_params['height']) {
									$GLOBALS['log'][] = "Compressing image..";
									$compressed = $this->compressImage($saved_file, null, $image_params['width'], $image_params['height'], $image_params['quality'], $image_params['crop']);
								}
								if (!empty($compressed)) {
									$GLOBALS['log'][] = "Compressed file: ".$compressed;
									unlink($saved_file);
									$saved_file = $compressed;
								}
	
							} catch (Exception $e) {
								Main::log("UploadFile -> getImageSize(): ".$e);
							}
						}
				}
				// path for DB entry
	
				$result['path'] = $destination.((substr($destination, -1) == "/") ? "" : "/").basename($saved_file);
				$GLOBALS['log'][] = "Result: ".$result['path'];
	
			} else $result['error'][] = "Uploading error";
				
		}
	
	
		return $result;
	}
	
	public function compressImage($original_image, $dest = null/* path for new file */, $width = null, $height = null, $quality = 85, $crop = false){
		$result = null;
		$new_image = null;
		$image = null;
	
		$info = getimagesize($original_image);
	
		if ($info['mime'] == 'image/jpeg') {
			$image = imagecreatefromjpeg($original_image);
		}
		elseif ($info['mime'] == 'image/gif') {
			$image = imagecreatefromgif($original_image);
		}
		elseif ($info['mime'] == 'image/png') {
			$image = imagecreatefrompng($original_image);
		}
		elseif ($info['mime'] == 'image/bmp') {
			$image = imagecreatefromwbmp($original_image);
		}
		else {
			$GLOBALS['log'][] = "CompressImage: Unknown image file format";
			return false;
			//$result['error'][] = 'Unknown image file format';
		}
	
		if (!$dest) {
			$filename_ending = "";
			if ($width) $filename_ending .= "-".$width;
			if ($height) $filename_ending .= "x".$height;
			if ($quality) $filename_ending .= "q".$quality;
				
			$new_name = md5($original_image.$filename_ending).".jpg";
			$dest = dirname($original_image)."/".$new_name;
				
		}
	
		$new_image = $image;
	
		if ($width || $height) {
			$old_width = $info[0];
			$old_height = $info[1];
				
			// calculate dimensions
			if ($width && !$height) {
				$height = floor($width * $old_height / $old_width);
			} elseif (!$width && $height) {
				$width = floor($height * $old_width / $old_height);
			}
				
			// Create a new blank image to hold the resized image
			$new_image = imagecreatetruecolor($width, $height);
	
			$old_ratio = $old_width / $old_height;
			$new_ratio = ($height > 0) ? $width / $height : 1;	// ?
				
			$adjusted_width = null;
			$adjusted_height = null;
			$width_margin = null;
			$height_margin = null;
				
			if ($crop) {
				// Resize and crop
				if ($old_ratio > $new_ratio) {
					// Crop the width
					$adjusted_width = floor( $old_width * ($height / $old_height) );
					$adjusted_height = $height;
					$width_margin = ($adjusted_width - $width) / 2;
				} elseif ($old_ratio < $new_ratio) {
					// Crop the height
					$adjusted_height = floor( $old_height * ($width / $old_width) );
					$adjusted_width = $width;
					$height_margin = ($adjusted_height - $height) / 2;
				}
	
				imagecopyresampled($new_image, $image, -$width_margin, -$height_margin, 0, 0, $adjusted_width, $adjusted_height, $old_width, $old_height);
			} else {
				// Resize it - no cropping needed
				imagecopyresampled($new_image, $image, 0, 0, 0, 0, $width, $height, $old_width, $old_height);
			}
		}
	
		if (!empty($new_image)) $new_image = imagejpeg($new_image, $dest, $quality);
			
		if ($new_image) {
			$result = $dest;
		} else return false;
	
	
		return $result;
	}
	
}


?>