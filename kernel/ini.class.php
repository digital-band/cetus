<?php 

class INI {
	
	public function set($filename, $key, $value){
		// DB_PREFIX = #.*
		if (file_exists($filename)) {
			$content = file_get_contents($filename);
			$updated = $this->setValue($key, $value, $content);
			try {
				file_put_contents($filename, $updated);
			} catch (Exception $e) {
				echo $e;
			}
		}
	}
	
	public function setList($filename, $entry){
		$content = '';
		$updated = '';
		
		if (file_exists($filename)) {
			$content = file_get_contents($filename);
			foreach ($entry as $key => $value) {
				$updated = $this->setValue($key, $value, $content);
				$content = $updated;
			}
			
			try {
				file_put_contents($filename, $updated);
			} catch (Exception $e) {
				echo $e;
			}
			
			$_SESSION['settings'] = parse_ini_file("config.ini");
		}
	}
	
	public function setValue($key, $value, $content){
		return preg_replace("/".$key." = .*/", $key." = ".$value, $content);
	}
	
}