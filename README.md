Setup your **config.ini**

```ini
; Main config

[App]

APPNAME = 
; subdomain
URL_PREFIX = /
MULTILANG = 1
DEFAULT_LANG = 1
CACHING = 1
DEBUGGING = 0

[DataBase]
DB_SERVER = 
DB_USER = 
DB_PASS = 
DB_NAME = 
DB_PREFIX = 
DB_CHARSET = utf8

[Security]
; Minimal access level for control panel. Less number more permissions. 1 is the highest permission.
MINLVL = 15
```