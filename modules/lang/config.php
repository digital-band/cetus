<?php 

$entry_points = Array(

);

$params = Array(
		"enable" => true,
		"in_cms" => true,
		"name" => "Language",	//  name in control panel
		"a_level" => 15,
		// if unexisted action requested the main action will handle it
		"redirect_action" => "main"
		//"cp_uri" => "path", //set custom path for module in control panel
);

// use for specific paths and controllers. %path after /admin/module_name/% => %controller%
$admin_points = Array(
		//"example/" => "example.php",
);

?>