<?php
/*
 *  Language module
*/
class LangCP extends CP {
	protected $sec_levels = Array();
	protected $obj;
	protected $mainObj;
	protected $id = null;

	public function __construct($main, $obj){
		// load basic features
		parent::__construct($main, $obj);
	}
	
	protected function action_main(){
		// get text tree grouped by module
		$list = $this->obj->getList(null, $this->order, $this->direction);
		$this->mainObj->setPageProperty('template', 'list');
		$this->mainObj->assign('list', $list);
	}
	
	protected function action_add(){
		// add lang
		$this->mainObj->setPageProperty('template', 'edit');
		$this->mainObj->setPageProperty('title', 'Add language');
	
		if ($this->mainObj->getUriParts(2) == 'add') {
			// add lang
			if (!empty($_POST)){
				$entry = $_POST;
				$new_id = $this->obj->add($entry);
	
				$this->mainObj->redirect($this->mainObj->getUriParts(0)."/".$this->mainObj->getUriParts(1)."/edit/".$new_id);
			}
	
		}
	}
	
	protected function action_edit() {
		// edit lang
		$this->mainObj->setPageProperty('template', 'edit');
		$this->mainObj->setPageProperty('title', 'Edit language');
		$id = $this->id;
		$entry = $this->entry =  $this->obj->get($id, true);
		
		if (!empty($_POST)){
			$this->obj->update($id, $_POST);
			$this->mainObj->redirect();
		}
		
		$this->mainObj->assign('entry', $this->entry);

	}
	
	
	protected function action_additem(){
		$this->mainObj->setPageProperty('template', 'edit_tag');
		// add text item
		if (!empty($_POST)){
			$entry = $_POST;
			$item = Array();
			$value = Array();
	
			$item['name'] = $entry['name'];
			$item['alias'] = $entry['alias'];
			$item['location'] = $entry['location'];
			// add text tag
			$new_id = $this->obj->addTag($item);
			if ($new_id){
				foreach ($entry['text'] as $k => $v) {
					// add values for each lang
					$value['tag_id'] = $new_id;
					$value['lang_id'] = $k;
					$value['text'] = $v;
	
					$this->obj->addValue($value);
				}
			}
			$this->mainObj->redirect($this->mainObj->getUriParts(0)."/".$this->mainObj->getUriParts(1)."/edititem/".$new_id);
		}
	}
	
	protected function action_edititem(){
		// edit text item
		$this->mainObj->setPageProperty('template', 'edit_tag');
		$id = $this->id;
		$entry = $this->entry = $this->entry = $this->obj->getTag($id, true);
		$value_list = $this->obj->getValueList($id);
		$entry['text'] = $value_list;
		
		if (!empty($_POST)){
			$entry = $_POST;
			$item = Array();
			$value = Array();

			$item['id'] = $id;
			$item['name'] = $entry['name'];
			$item['alias'] = $entry['alias'];
			$item['location'] = $entry['location'];
			
			// add text item
			$this->obj->updateTag($id, $item);
			foreach ($entry['text'] as $k => $v) {
				// add values for each lang
				$value = Array();
				$value['tag_id'] = $id;
				$value['lang_id'] = $k;
				$value['text'] = $v;
				
				$item['value'] = $value;
				// update if value alredy exists
				if (!empty($value_list[$k])) {
					$value['id'] = $value_list[$k]['id'];
					$this->obj->updateValue($value['id'], $value);
				} else {
					// otherwise create new
					$this->obj->addValue($value);
				}
			}	
			/*
			// save to cache
			echo "lang[tag_collection][".$value['lang_id']."][".$item['location']."][".$entry['alias']."]<br>";
			?><pre><?php print_r($item) ?></pre><?
			$this->cache->_set("lang[tag_collection][".$value['lang_id']."][".$item['location']."][".$entry['alias']."]", $item);
			*/
			$this->mainObj->redirect();
		}
		$this->mainObj->assign('entry', $entry);
	}
	
	protected function action_delitem(){
		// delete tag and its values
		$id = $this->id;
		$entry = $this->obj->getTag($id, true);
		
		if (!empty($entry)) {
			$this->obj->deleteValuesByTag($id);
			$this->obj->deleteTag($id);
		}
		
		$this->mainObj->redirect($this->mainObj->getUriParts(0)."/".$this->mainObj->getUriParts(1));
		
	}
	
	protected function action_del(){
		// delete language and its values
		$id = $this->id;
		$entry = $this->obj->get($id, true);
	
		if (!empty($entry)) {
			$this->obj->deleteValuesByLang($id);
			$this->obj->delete($id);
		}
	
		$this->mainObj->redirect($this->mainObj->getUriParts(0)."/".$this->mainObj->getUriParts(1));
	}

	
	
}