<?php 
/* This module created for all front-end text management 
 * 
 */
class Lang extends Module{
	protected $classname = "lang";
	protected $modulename = "Language";
	
	protected $mainObj;
	protected $dbObj;
	protected $cache;

	private $lang_id = null;
	private $location = '/';
	private $root = '';
	private $tag_collection = Array();
	private $alias_collection = Array();
	
	protected $tables = Array(
		// describes db structure
		'cats' => Array(
			'table' => 'lang',
			'fields' => Array(
				'name' => Array('type' => "string"),
				'priority' => Array('type' => "int"),
				'public' => Array('type' => "int"),
			),
		),
			
		'items' => Array(
			'table' => 'lang_tags',
			'fields' => Array(
				'name' => Array('type' => "string"),
				'alias' => Array('type' => "string"),
				'location' => Array('type' => "string"),
				'public' => Array('type' => "int"),
			),
		),
			
		'values' => Array(
			'table' => 'lang_values',
			'fields' => Array(
				'tag_id' => Array('type' => "int"),
				'lang_id' => Array('type' => "int"),
				'text' => Array('type' => "string"),
			),
		),
	);
	
	public function __construct($mainObj){
		parent::__construct($mainObj);
	}
	
	//-------------		FOR CP	-------------
	public function getTopMenu(){
		// loads module control panel menu top level
		return $this->getLangList(true);
	}
	//------------- 	GET 	-------------
	public function getList($hidden = 0, $order = null, $direction = null){
		// get text tag list
		return $this->dbObj->fetchAll($this->dbObj->select('items',  (($hidden) ? ' 1 ' : " `public` = 1" ), 
				(($order) ? $order : '`location`' ),
				(($direction) ? $direction : null )
			)
		);
	}
	
	public function getValueList($id){	// by tag id
		$result = $this->dbObj->fetchAll($this->dbObj->select('values',  "`tag_id` = ".$id ));
		$return = Array();
		foreach ($result as $k => $v) {
			$return[$v['lang_id']] = $v;
		}
		
		return $return;
	}
	
	public function getListByLang($cat_id){
		return $this->dbObj->fetchAll($this->dbObj->select('items', "`lang_id` = ".$cat_id));
	}
	
	public function getLangList($hidden = false){
		$result = Array();
		// try to get from cache
		$result = $this->cache->_get("lang[lang_list]");
		if (empty($result)){
			$result = $this->dbObj->fetchAll($this->dbObj->select('cats', (($hidden) ? ' 1 ' : " `public` = 1" ), "`priority`"), "id");
			$this->cache->_set("lang[lang_list]", $result);
		}
		
		return $result;
	}
	
	public function getTag($id, $hidden){
		// get text tag
		if (is_numeric($id)) { // if id
			$entry = $this->dbObj->select('items', "`id` = ".$id.(($hidden) ? '' : " AND `public` = 1"))->fetch_assoc();
		} else return false;
		
		return $this->dbObj->restoreQ($entry);
	}
	
	public function pargseTag($alias, $lang_id, $location = "", $hidden = 0){
		// get text tag value for specific language
		if (substr($location, -1) != '/') {
			$location .= '/';
		}
		$query = ((is_numeric($alias)) ? "`id`" : "`alias`")." = '".$alias."'".(($hidden) ? '' : " AND `public` = 1")." 
				AND `location` LIKE LEFT('$location', 7)";

		$entry = $this->dbObj->query("SELECT * FROM ".DB_PREFIX.'items'." AS tmp
				WHERE ".((is_numeric($alias)) ? "`id`" : "`alias`")." = '".$alias."'".(($hidden) ? '' : " AND `public` = 1")."
				AND
				EXISTS (SELECT tmp.location FROM ".DB_PREFIX.'items'." AS tmp2 WHERE tmp.location LIKE LEFT('$location', LENGTH(tmp2.location)) OR tmp.location = '$this->root')
				"
		)->fetch_assoc();
		
		//$entry = $this->dbObj->select('items', $query)->fetch_assoc();
		if (!empty($entry)) $entry['value'] = $this->dbObj->select('values', "`tag_id` = ".$entry['id']." AND `lang_id` = ". $lang_id)->fetch_assoc();
		
		return $this->dbObj->restoreQ($entry);
	}
	
	public function pargseTagList($alias_list, $lang_id, $location = "", $full = 1, $hidden = 0 ){
		// $full = 1 means the query will request full list of tags for the current page even if they are invisible (dynamic page)
		// check location
		//if (!($location = $this->dbObj->validateInput($location))) return null;
		$result = Array();
		// get text tag value for specific language
		if (substr($location, -1) != '/') {
			$location .= '/';
		}
		if ($full) {
			$query = "SELECT * FROM ".DB_PREFIX.$this->tables['items']['table']." AS tmp
					WHERE 1".(($hidden) ? '' : " AND `public` = 1")."
					AND
					EXISTS (SELECT tmp.location FROM ".DB_PREFIX.$this->tables['items']['table']." AS tmp2
							WHERE tmp.location LIKE LEFT('$location', LENGTH(tmp2.location)) OR tmp.location = '$this->root')
							"
							;
		} else {
			$query = "SELECT * FROM ".DB_PREFIX.$this->tables['items']['table']." AS tmp
					WHERE ".((is_numeric($alias_list)) ? "`id`" : "`alias`")." IN (".$alias_list.")".(($hidden) ? '' : " AND `public` = 1")."
					AND
					EXISTS (SELECT tmp.location FROM ".DB_PREFIX.$this->tables['items']['table']." AS tmp2 
					WHERE tmp.location LIKE LEFT('$location', LENGTH(tmp2.location)) OR tmp.location = '$this->root')
					"
				;
		}
		$result = $this->dbObj->fetchAll($this->dbObj->query($query));

		if (!empty($result)) {
			$tag_ids = Array();
			$values = Array();
			
			$key_list = Array();
			$key_values = Array();

			$result = $this->mainObj->uniqueByPriority($result, "name", "location");

			foreach ($result as $item) {
				$tag_ids[] = $item['id'];
				$key_list[$item['id']] = $item;
			}
			
			$id_list = implode(', ', $tag_ids);
			
			$values = $this->dbObj->fetchAll($this->dbObj->select('values', "`tag_id` IN (".$id_list.") AND `lang_id` = ". $lang_id));
			
			$key_values = $this->mainObj->reKey($values, "tag_id");
			
			foreach ($key_list as $k => $v){
				$key_list[$k]['value'] = $key_values[$k];
			}

			if (!empty($key_list)) $result = $this->dbObj->restoreQ($key_list);

		}
	
		return $result;
	}

	
	public function get($id, $hidden){
		// get language
		if (is_numeric($id)) { // if id
			$entry = $this->dbObj->select('cats', "`id` = ".$id.(($hidden) ? '' : " AND `public` = 1"))->fetch_assoc();
		} else return false;
	
		return $this->dbObj->restoreQ($entry);
	}

	
	//------------- 	EDIT 	-------------
	public function addTag($entry){
		// add new tag
		$new_id = $this->dbObj->insert('items', $entry);
	
		return $new_id;
	}
	
	public function addValue($entry){
		// add value for a tag
		$new_id = $this->dbObj->insert('values', $entry);
	
		return $new_id;
	}
	
	public function add($entry){
		// add new language
		$new_id = $this->dbObj->insert('cats', $entry);

		return $new_id;
	}
	
	
	public function updateTag($id, $entry){
		// update tag
		$result = $this->dbObj->update('items', $entry, "`id` = ".$id);
	}
		
	public function updateValue($id, $entry){
		$result = $this->dbObj->update('values', $entry, "`id` = ".$id);
	}
	
	public function update($id, $entry){
		$result = $this->dbObj->update('cats', $entry, "`id` = ".$id);
	}
	
	
	public function deleteTag($id){
		// del tag
		$this->dbObj->delete('items', "`id` = ".$id);
	}

	public function deleteValue($id){
		// del value
		$this->dbObj->delete('values', "`id` = ".$id);
	}
	
	public function delete($id){
		// del lang
		$this->dbObj->delete('cats', "`id` = ".$id);
	}
	
	public function deleteValuesByTag($id){
		// del value list
		$this->dbObj->delete('values', "`tag_id` = ".$id);
	}
	
	public function deleteValuesByLang($id){
		// del value list
		$this->dbObj->delete('values', "`lang_id` = ".$id);
	}
	
	
	//_____________  OPERATIONS _____________
	
	public function request_tag($text) {
		// @depricated
		// parses each tag one by one. Heavy.
		$value = '';
		if (!empty($text)) {
			$alias = substr($text[0], 2, -2);
			if (strlen($alias)) {
				$value = $this->pargseTag($alias, $this->lang_id, $this->location);

				$value = $value['value']['text'];
			}
		}
		
		return $value;
	}
	
	public function request_tag_from_array($text) {
		// Collects all tags in array then parses them in one query
		// This callback must find tag value in array, not in DB
		$value = '';
		if (!empty($text)) {
			$alias = substr($text[0], 1, -1);
			if (strlen($alias)) {
				if (key_exists($alias, $this->tag_collection)) {
					$value = $this->tag_collection[$alias]['value']['text'];
				} else {
					$value = $text[0];
				}
			}
		}	

		return $value;
	}
	
	public function parse($raw, $lang_id, $location){
		$this->lang_id = $lang_id;
		
		$safe_location = $this->dbObj->escape($location, false);	
		$this->location = $safe_location;
		$result = null;
		
		// looking for unique tags 
		$tags = Array();

		$mask = '#\[[a-zA-Z\d\_]*\]#';
		
		preg_match_all($mask, $raw, $tags);
		$tag_list = array_unique($tags[0]);
		
		// extract aliases

		if (!empty($tag_list)){
			foreach ($tag_list as $tag){
				$alias = substr($tag, 1, -1);
				
				if (strlen($alias)) {
					if (!in_array($alias, $this->alias_collection)) $this->alias_collection[] = $alias;
				}
			}
		}
		// request tag values
		
		$alias_list = "'".implode("', '", $this->alias_collection)."'";
		// try to get from cache
		$cached_tag_collection = $this->cache->_get("lang[tag_collection][$lang_id][$location]");
		
		if (isset($cached_tag_collection)) {
			$this->tag_collection = $cached_tag_collection;	
		} else {
			// check location
			//if ($location = $this->dbObj->validateInput($location)) {
				// request new data
				$values = $this->pargseTagList($alias_list, $this->lang_id, $this->location, 1);
				$this->tag_collection = $this->mainObj->reKey($values, 'alias');
				// save to cache
				$this->cache->_set("lang[tag_collection][$lang_id][$location]", $this->tag_collection);
			//}
		}

		$result = preg_replace_callback($mask, array($this, 'request_tag_from_array'), $raw);

		return $result;
	}
	
	
	
	
}

?>