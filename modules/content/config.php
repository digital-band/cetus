<?php 

$params = Array(
		"enable" => true,
		// show in admin panel
		"in_cms" => true,
		// title in admin panel
		"name" => "Content",
		// min user level to access
		"a_level" => 15,
		// if unexisted action requested the main action will handle it
		"redirect_action" => "main"
		//"cp_uri" => "path", //set custom path for module in control panel
);

/* use for specific paths and controllers. %path after module_name/% => %controller%
 * for exapmle
 * "registration/" => "reg.php",
 * "registration/event/" => "event.php",
 */
/*
$entry_points = Array(
	"exam/" => "exam.php",
);
*/
/* use for specific paths and controllers. %path after /admin/module_name/% => %controller%
 * 
 * for example
 * "add/" => "add.php",
*/
$admin_points = Array(
		
);

?>