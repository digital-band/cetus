<?php 

class ContentController extends Controller{
	protected $obj;
	protected $mainObj;
	protected $template = null;
	// set access levels
	protected $sec_levels = Array(
		"send" 		=> 20,
	);

	public function __construct($mainObj, $moduleObj){	
		// load basic features
		parent::__construct($mainObj, $moduleObj);
	}
	
	protected function action_main(){
		// Get requested page by uri
		$this->mainObj->setPage($this->obj->get($this->mainObj->getUri(), 0));
		
		if ($this->mainObj->getPageProperty('id')) {
			// load specific controller for the page if required
			if ($controller = $this->mainObj->getPageProperty('controller')) {
				require $controller['path'];
				new $controller['classname']($this->mainObj, $this->obj);
			}
		} else $this->mainObj->setPage(Main::error(404));

	}
	
	protected function action_send(){
		$GLOBALS['log'][] = "Content: send action";
	}
	
	protected function action_cache(){
		if (!empty($_GET['mod'])) {
			$module = $_GET['mod'];
			if (!empty($module)) {
				$cache = $this->obj->getCacher()->get($module);
				$GLOBALS['log'][] = "Module $module cached data: ";
				$GLOBALS['log'][] = $cache;
			}
		}
	}
}





?>