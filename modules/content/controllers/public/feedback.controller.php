<?php 

class FeedbackController extends Controller{
	protected $obj;
	protected $mainObj;
	protected $template = null;
	
	function __construct($mainObj, $moduleObj){
		// load basic features
		parent::__construct($mainObj, $moduleObj);
		
		$GLOBALS['log'][] = 'feedback.controller.php';
	}
	
	public function action_main(){
		$GLOBALS['log'][] = "Action Main";
	}
	
	public function action_show(){
		$GLOBALS['log'][] = "Action Show";
	}
	
	public function action_send(){
		$GLOBALS['log'][] = "Action Send";
	}
	
	public function action_list(){
		$GLOBALS['log'][] = "Action List";
	}
}

?>