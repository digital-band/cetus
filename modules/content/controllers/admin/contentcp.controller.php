<?php 
/*
 *  Content module 
 */
class ContentCP extends CP{
	protected $sec_levels = Array();
	protected $obj;
	protected $mainObj;
	protected $id = null;
	
	public function __construct($mainObj, $moduleObj){
		// load basic features
		parent::__construct($mainObj, $moduleObj);
	}
	
	protected function action_main(){

	}

	protected function action_add(){
		$parent = @intval($_GET['parent']);

		if (!empty($parent)){
			$parent = $this->obj->get($parent, true);
			$this->mainObj->assign('parent', $parent);

		}
		
		// save
		if (!empty($_POST)){
			$entry = $_POST;
			$new_id = $this->obj->add($entry);
			$this->mainObj->redirect($this->mainObj->getUriParts(0)."/".$this->mainObj->getUriParts(1)."/edit/".$new_id);
		}
		
		$this->mainObj->setPageProperty('title', "Add page");
		$this->mainObj->setPageProperty('template', 'edit');
	}
	
	protected function action_edit(){
		$entry = $this->entry = $this->obj->get($this->id, true);

		if (!empty($entry['parent'])) {
			$parent = $this->obj->get($entry['parent'], true);
			$this->mainObj->assign('parent', $parent);
		}
		
		$this->mainObj->setPageProperty('title', $this->mainObj->getPageProperty('title').$entry['title']);
		$this->mainObj->setPageProperty('template', 'edit');
			
		// save
		if (!empty($_POST)){
			$entry = $_POST;
			$this->obj->update($this->id, $entry);
			$this->mainObj->redirect();
		}
		
		$this->mainObj->assign('entry', $entry);
		$this->mainObj->setPageProperty('title', "Edit page");
		$this->mainObj->setPageProperty('template', 'edit');
	}
	
	protected function action_delete(){	
		$this->obj->delete($this->id);
		$this->mainObj->redirect($this->mainObj->getUriParts(0)."/".$this->mainObj->getUriParts(1));
	}
	
}





?>