<?php 

class Content extends Module{
	protected $classname = "content";
	protected $modulename = "Content";
	
	protected $mainObj;
	protected $dbObj;
	protected $cache;
	
	private $page_properties = Array(
	
	);
	
	protected $tables = Array(
		// describes db table structure
		'items' => Array(
			'table' => 'content',
			'fields'=> Array(
				'parent' => Array('type' => "int"),
				'title' => Array('type' => "string"),
				'name' => Array('type' => "string"),
				'uri' => Array('type' => "string"),
				'h1' => Array('type' => "string"),
				'text' => Array('type' => "string"),
				'template' => Array('type' => "string"),
				'controller' => Array('type' => "string"),
				'keywords' => Array('type' => "string"),
				'description' => Array('type' => "string"),
				'public' => Array('type' => "int"),
			)
		),
	);
	
	public function __construct($mainObj){
		parent::__construct($mainObj);
	}
	
	// getters {

	// getters }
	
	// setters {

	// setters }
	
	//-------------		FOR CP	-------------
	public function getTopMenu(){
		// loads module control panel menu top level
		$cache = $this->cache->_get("content[tree]");
		if (!empty($cache)) {
			$topmenu = $cache;
		} else {
			$topmenu = $this->getTree(0);
			$this->cache->_set("content[tree]", $topmenu);
		}

		foreach ($topmenu as $k => $v) {
			if (!empty($v['title'])) {
				// clean of lang tag breakets 
				if (substr($v['title'], 0, 1) == "[" && substr($v['title'], -1) == "]") {
					$topmenu[$k]['title'] = substr($v['title'], 1, -1);
				}
			}
		}

		return $topmenu;
	}
	//------------- 	GET 	-------------
	
	public function getList($parent){
		// get list of pages 
		$result = Array();
		$cache = $this->cache->_get("content[lists][".$parent."]");
		if (empty($cache)) {
			$result = $this->dbObj->fetchAll($this->dbObj->select('items', "`parent` = ".$cat_id));
		} else $result = $cache;
		
		
		return $result;
	}
	
	public function getTree($parent){
		// get tree of pages starting from $parent; also it's top level menu function
		$list = $this->dbObj->fetchAll($this->dbObj->select('items', "`parent` = ".$parent));
		
		if (!empty($list)) {
			foreach ($list as $k => $value){
				$list[$k]['childNodes'] = $this->getTree($value['id']);			
			}
		}
		return $list;	
	}
	
	public function getTreeReverse($id){
		// get tree of pages starting from current child
		$list = Array();
		
		$entry = $this->get($id, 1);
		$list[] = $entry;
		
		while (intval($entry['parent']) > 0){
			$entry = $this->get($entry['parent'], 1);
			$list[] = $entry;
		}

		return $list;
	}
	
	public function getController($id){
		// loads specific controller for page if required
		$result = Array();
		$parent_tree = $this->getTreeReverse($id);
		$k = 0;

		while (empty($result['path']) && $k < count($parent_tree)) {		
			$file = MODULE_DIR.$this->classname."/controllers/public/".$parent_tree[$k]['controller'].".controller.php";

			if (file_exists($file)) {
				$result['path'] = $file;
				$result['classname'] = $parent_tree[$k]['controller']."Controller";
			}
			$k++;
		}
		
		return $result;
	}
	
	public function getContentTreeList($parent){
		// get simple list of childs
		$list = $this->dbObj->fetchAll($this->dbObj->select('items', "`parent` = ".$parent));
		if (!empty($list)) {
			foreach ($list as $k => $value){
				$list = array_merge($list, $this->getContentTreeList($value['id']));
			}
		}
		return $list;
	}
	
	
	public function getDirectly($path, $hidden = 0){
		// get page
		if (is_numeric($path)) { // if id
			$entry = $this->dbObj->fetch($this->dbObj->select('items', "`id` = ".$path.(($hidden) ? '' : " AND `public` = 1")));
		} else {
			$query = 	'(`uri` = "'.preg_replace("'/'", "", preg_replace("#/[/]*$#", "", $path, 1), 1).'/" OR  
						  `uri` = "'.preg_replace("'/'", "", preg_replace("#/[/]*$#", "", $path, 1), 1).'" ) '.(($hidden) ? '' : " AND `public` = 1");
			$entry = $this->dbObj->fetch($this->dbObj->select('items', $query));
		}
		
		return $this->dbObj->restoreQ($entry);
	}
	
	public function get($path, $hidden = 0){
		// check input string
		if (!is_numeric($path)) if (!($path = $this->dbObj->validateInput($path))) return null;
		
		// get page from cache firstly
		$result = Array(); 
		// Check cache
		$cache = $this->cache->_get("content[pages][".$path."]");
		// if id get directly
		if (empty($cache)) {
			if (is_numeric($path)) {
				$result = $this->dbObj->fetch($this->dbObj->select('items', "`id` = ".$path.(($hidden) ? '' : " AND `public` = 1")));
			} else {
				
				$safe_path = $this->dbObj->safeQ($path);
				$safe_path = preg_replace("#/[\/]*$#", "", $safe_path, 1);
			
				$query = 	'(`uri` = "'.$safe_path.'" OR
						 	  `uri` = "'. $safe_path.'/" ) '.(($hidden) ? '' : " AND `public` = 1");
				$result = $this->dbObj->fetch($this->dbObj->select('items', $query));
				$result = $this->dbObj->restoreQ($result);
				
				if (!empty($result['id'])) $result['controller'] = $this->getController($result['id']);
			}
			// save to cache
			$this->cache->_set("content[pages][".$path."]", $result);
		} else {
			// return cache
			$result = $cache;
		}

		return $result;
	}

	
	//------------- 	EDIT 	-------------
	
	
	public function add($entry){
		// add new page of content
		$new_id = $this->dbObj->insert('items', $entry);

		return $new_id;
	}
	
	public function update($id, $entry){
		// edit page of content
		$old = $this->get($id, 1);
		if ($old['public'] != $entry['public']) {
			// update child nodes visibility
			$childs = $this->getContentTreeList($id);		
			$ids = Array();
			$new_entry['public'] = $entry['public'];
			foreach ($childs as $v) {
				$ids[] = $v['id'];	
			}
			
			if (!empty($ids)) $this->dbObj->update('items', $new_entry, "`id` IN (".implode(',', $ids).")");
			
		}
		
		$result = $this->dbObj->update('items', $entry, "`id` = ".$id);
		
		if (!empty($entry)) {
			// save to cache
			$entry['id'] = $id;
			if (!empty($entry['uri'])) $this->cache->_set("content[pages][".$entry['uri']."]", $entry);
			$this->cache->_set("content[pages][".$id."]", $entry);
			$this->cache->_set("content[tree]", null);
		}
	} 
	
	public function delete($id){
		// del page of content and its childs
		$entry = $this->get($id);
		$childs = $this->getContentTreeList($id);
		$ids = Array();
		foreach ($childs as $v) {
			$ids[] = $v['id'];
		}

		if (!empty($ids)) $this->dbObj->delete('items', "`id` IN (".implode(',', $ids).")");
		$deleted = $this->dbObj->delete('items', "`id` = ".$id);
		
		if ($deleted) {
			// save to cache
			$this->cache->_set("content[pages][".$entry['uri']."]", null);
			$this->cache->_set("content[pages][".$id."]", null);
			$this->cache->_set("content[tree]", null);
		}
		
	}

}

?>