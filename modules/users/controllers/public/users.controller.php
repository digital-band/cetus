<?php 

class UsersController extends Controller{
	protected $obj;
	protected $mainObj;
	protected $template = null;
	// set access levels
	protected $sec_levels = Array(
		"send" 		=> 20,
		"logout" 	=> 50,
	);
		
	public function __construct($mainObj, $moduleObj){	
		// load basic features
		parent::__construct($mainObj, $moduleObj);
	}
	
	protected function action_main(){
		$this->template = "login";
		
	}
	
	protected function action_login(){
		$this->template = "login";
		//echo "Login action<br>";
		$GLOBALS['log'][] = $_POST;
		$GLOBALS['log'][] = $_GET;
		
		if (!empty($_POST['id']) && !empty($_POST['pass'])) {
			$user = $this->obj->login($_POST);
			if (!empty($user['id'])) {
				$this->mainObj->assign('user', $user);	
				$redirect = "";
				if (!empty($_SESSION['user_redirect_link'])) {
					$redirect = $_SESSION['user_redirect_link'];
					//unset($_SESSION['user_redirect_link']);
				}
				Main::redirect((!empty($redirect)) ? $redirect : "users/");
				
			}
		} else $this->mainObj->assign('login_data', $_POST);
		
	}
	
	protected function action_logout(){
		$this->template = "login";
		$this->obj->logout();
		$this->mainObj->redirect("users/");
	}
	
	protected function action_registration(){
		$GLOBALS['log'][] = "Registration action";
		$this->template = "login";
	}
	
	protected function action_send(){
		$GLOBALS['log'][] = "Send action. Only for registred users.";
		$this->template = "login";
	
	}

	
}





?>