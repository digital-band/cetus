<?php 

class UsersCP extends CP{
	protected $obj;
	protected $mainObj;
	protected $id = null;
	// set access levels
	protected $sec_levels = Array(
		"main" 			=> 15,
		"additem" 		=> 10,
		"edititem" 		=> 10,
		"deleteitem" 	=> 10,
		"add" 			=> 10,
		"edit" 			=> 10,
		"del" 			=> 10,
	);
	
	public function __construct($mainObj, $moduleObj){
		// load basic features
		parent::__construct($mainObj, $moduleObj);
	}
	protected function action_main(){
		$list = $this->obj->getList(null, $this->order, $this->direction);
		$this->mainObj->assign('list', $list);
	}
	// User	{
	protected function action_additem(){
		$category = @$_GET['category'];
		$langObj = $this->mainObj->load("lang", $this->mainObj);
		$lang_list = $langObj->getLangList();
		$cat_tree = $this->obj->getTree();
		
		if (!empty($category)){
			$category = $this->obj->get($category);
			$this->mainObj->assign('category', $category);
		}
		
		// save
		if (!empty($_POST)){
			$entry = $_POST;
			$new_id = $this->obj->add($entry);
			if (is_numeric($new_id)) {
				$this->mainObj->redirect($this->mainObj->getUriParts(0)."/".$this->mainObj->getUriParts(1)."/edititem/".$new_id);
			} else {
				if (!empty($new_id['error'])) {
					// error
					$GLOBALS['log'][] = "Error: ".Main::Print_r($new_id['error']);
					$this->mainObj->assign('error', $new_id['error']);
					$this->mainObj->assign('entry', $new_id);
				}
			}
			
		}
		
		$this->mainObj->setPageProperty('title', "Add user");
		$this->mainObj->setPageProperty('template', 'edititem');
		$this->mainObj->assign('lang_list', $lang_list);
		$this->mainObj->assign('cat_tree', $cat_tree);
	}
	
	protected function action_edititem(){
		$entry = $this->entry = $this->obj->get($this->id, true);
		$langObj = $this->mainObj->load("lang", $this->mainObj);	
		$lang_list = $langObj->getLangList();
		$cat_tree = $this->obj->getTopMenu();
		// save
		if (!empty($_POST)){
			$result = $this->obj->update($this->id, $_POST);
			if ($result === true) {
				if ($_SESSION['user']['id'] == $this->id) {
					$_SESSION['user'] = $this->obj->get($this->id);
				}
				$this->mainObj->redirect();
			} else {
				if (!empty($result['error'])) {
					// error
					$GLOBALS['log'][] = "Error: ".Main::print_r($result['error']);
					$this->mainObj->assign('error', $result['error']);
					$this->mainObj->assign('entry', $result);
				}
			}
			
		}
		
		$this->mainObj->setPageProperty('title', "Edit user");
		$this->mainObj->setPageProperty('template', 'edititem');

		$this->mainObj->assign(Array(
				'entry' => $entry,
				'lang_list' => $lang_list,
				'cat_tree' => $cat_tree,
		));
	}
	
	protected function action_deleteitem(){	
		$this->obj->delete($this->id);
		$this->mainObj->redirect($this->mainObj->getUriParts(0)."/".$this->mainObj->getUriParts(1));
	}
	// User	}
	
	// Category	{
	
	protected function action_add(){
		$parent = @$_GET['parent'];
		if (!empty($parent)){
			$parent = $this->obj->getCategory($parent);
			$this->mainObj->assign('parent', $parent);
		}
		// save
		if (!empty($_POST)){
			$entry = $_POST;
			$new_id = $this->obj->addCategory($entry);
			$this->mainObj->redirect($this->mainObj->getUriParts(0)."/".$this->mainObj->getUriParts(1)."/edit/".$new_id);
		}
		
		$this->mainObj->setPageProperty('title', "Add category");
		$this->mainObj->setPageProperty('template', 'edit');
	}
	
	protected function action_edit(){
		// edit category
		$entry = $this->entry = $this->obj->getCategory($this->id);
		$list = $this->obj->getList($this->id, $this->order, $this->direction);

		// save
		if (!empty($_POST)){
			$entry = $_POST;
			$this->obj->updateCategory($this->id, $entry);
			$this->mainObj->redirect();
		}
		
		$this->mainObj->setPageProperty('title', "Edit category");
		$this->mainObj->setPageProperty('template', 'edit');
		$this->mainObj->assign('entry', $entry);
		$this->mainObj->assign('parent', $entry['parent_info']);
		$this->mainObj->assign('list', $list);
	}
	
	protected function action_del(){
		$this->obj->deleteCategory($this->id);
		$this->mainObj->redirect($this->mainObj->getUriParts(0)."/".$this->mainObj->getUriParts(1));
	}
	// Category	}
}





?>