<?php 

$params = Array(
	"enable" => true,
	"in_cms" => true,
	"name" => "Users",
	"a_level" => 20,	// access level
	//"cp_uri" => "path", //set custom path for module in control panel
);

/* use for specific paths and controllers. %path after module_name/% => %controller%
 * for exapmle
 * "registration/" => "reg.php",
 * "registration/event/" => "event.php",
 */

$entry_points = Array(
	
);

/* use for specific paths and controllers. %path after /admin/module_name/% => %controller%
 * 
 * for example
 * "add/" => "add.php",
*/
$admin_points = Array(
		
);

?>