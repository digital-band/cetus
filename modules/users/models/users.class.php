<?php 

class Users extends Module{
	protected $classname = "users";
	protected $modulename = "Users";
	
	protected $mainObj;
	protected $dbObj;
	protected $cache;

	protected $tables = Array(
		// describes db table structure
		'cats' => Array(
			'table' => "users_cat",
			'fields' => Array(
				'parent' => Array('type' => "int"),
				'level' => Array('type' => "string"),
				'name' => Array('type' => "string"),
			),
		),
		
		'items' => Array(
			'table' => "users",
			'fields' => Array(
				'category' => Array('type' => "int"),
				'login' => Array('type' => "string"),
				'name' => Array('type' => "string"),
				'middlename' => Array('type' => "string"),
				'lastname' => Array('type' => "string"),
				'email' => Array('type' => "string"),
				'pass' => Array('type' => "string"),
				'lang_id' => Array('type' => "int"),
				'image' => Array('type' => "string"),
			),
		)
	);

	
	public function __construct(Main $mainObj){
		parent::__construct($mainObj);
	}
	
	// getters {
	
	// getters }
	
	// setters {
	
	// setters }
	
	//-------------		FOR CP	-------------
	public function getTopMenu(){
		// loads top level menu of module
		$result = Array();
		$cache = $this->cache->_get("users[tree]");
		if (!empty($cache)) {
			$result = $cache;
		} else {
			$result = $this->getTree(0);
			$this->cache->_set("users[tree]", $result);
		}
		
		return $result;
	}
	//------------- 	GET 	-------------
	
	public function getList($category = null, $order = null, $direction = null){
		// get list of users
		$result = Array();
		/*
		// fetch info from cache
		if ($category) {
			// list for category
			$cache = $this->cache->_get("users[categories][".$category."][items]");
			if (!empty($cache)) $result = $cache;
		} else {
			// full list
			$cache = $this->cache->_get("users[items]");
			if (!empty($cache)) {
				if ($order && !is_null($direction)) {
					$cache = Main::order($cache, $order, $direction);	
				}
				$result = $cache;
			}
		}
		*/
		// fetch info from database
		if (empty($result)) {
			$result = $this->dbObj->fetchAll($this->dbObj->select(
					'items',
					(($category) ? "`category` = $category" : null ),
					(($order) ? $order : null ),
					(($direction) ? $direction : null )
				)
			);
			if (!empty($result)) {
				foreach ($result as $k=>$v){
					$result[$k] = $this->parseItem($v);
					/*if ($category) {
						$this->cache->_set("users[categories][".$result[$k]['category']."][items][".$result[$k]['id']."]", $result[$k]);
					} else {
						$this->cache->_set("users[items][".$result[$k]['id']."]", $result[$k]);
					}*/
					$this->cache->_set("users[items][".$result[$k]['id']."]", $result[$k]);
				}
			}
		}
		

		return $result;
	}

	
	public function getTree($parent = 0, $deep = 0){
		// get tree of user categories starting from $parent; also it's top level menu fucntion
		$list = $this->dbObj->fetchAll($this->dbObj->select('cats', "`parent` = ".$parent, "level, name"));
		if (!empty($list)) {
			foreach ($list as $k => $value){
				$list[$k]['childNodes'] = $this->getTree($value['id']);		
				// get item list for this category
				if ($deep) $list[$k]['items'] = $this->getList($value['id']);			
			}
		}
		return $list;	
	}
	
	public function getCategoryList($parent = 0){
		// get simple list
		$list = $this->dbObj->fetchAll($this->dbObj->select('cats', "`parent` = ".$parent));
		if (!empty($list)) {
			foreach ($list as $k => $value){
				$list = array_merge($list, $this->getCategoryList($value['id']));
			}
		}
		return $list;
	}
	
	
	public function get($id){
		// get user
		$entry = $this->cache->_get("users[items][".$id."]");
		if (empty($entry['id'])) {
			if (is_numeric($id)) { 
				// if id
				$entry = $this->dbObj->fetch($this->dbObj->select('items', "`id` = ".$id));
				
			} elseif (preg_match("#@[A-z]*\.#", $id)) {
				// if email
				$id = $this->dbObj->safeQ($id);
				$entry = $this->dbObj->select('items', "`email` = '".$id."'")->fetch_assoc();
			} else {
				// if login
				$id = $this->dbObj->safeQ($id);
				$entry = $this->dbObj->select('items', "`login` = '".$id."'")->fetch_assoc();
			}
			$entry = $this->dbObj->restoreQ($entry);
			if (!empty($entry)) {
				$entry = $this->parseItem($entry);
				$this->cache->_set("users[items][".$entry['id']."]", $entry);
				//$this->cache->_set("users[categories][".$entry['category']."][items][".$entry['id']."]", $entry);
			}
		}
			
		return $entry;
	}

	
	public function getCategory($id, $deep = true){
		$result = null;
		$cache = $this->cache->_get("users[categories][".$id."]");
		if (!empty($cache['id'])) {
			$result = $cache;
		} else {
			$result = $this->dbObj->fetch($this->dbObj->select('cats', "`id` = ".$id));
			$result['parent_info'] = '';
			if ($deep) $result['childNodes'] = $this->getTree($id);
			
			$this->cache->_set("users[categories][".$id."]", $result);
		}
		
		if (!empty($result['parent'])) {
			$result['parent_info'] = $this->getCategory($result['parent'], false);
		}

		return $result;
	}

	
	//------------- 	EDIT 	-------------
	
	
	public function add($entry){
		// add new user
		$result = null;
		$entry = $this->prepare($entry);
		if (empty($entry['error'])) {
			$result = $this->dbObj->insert('items', $entry);
		} else {
			$result = $entry;
		}
		
		return $result;
	}
	
	public function addCategory($entry){
		$new_id = $this->dbObj->insert('cats', $entry);
		$this->cache->_set("users[tree]", null);
	
		return $new_id;
	}
	
	public function update($id, $entry){
		// edit user
		$result = false;
		$entry = $this->prepare($entry, $id);
		if (empty($entry['error'])) {
			$this->dbObj->update('items', $entry, "`id` = ".$id);
			$result = true;
			$this->cache->_set("users[items][".$id."]", null);
		} else {
			$result = $entry;
		}
		
		
		return $result;
	}

	public function updateCategory($id, $entry){
		$entry = $this->prepareCategory($id, $entry);
		$result = $this->dbObj->update('cats', $entry, "`id` = ".$id);
		$this->cache->_set("users[categories][".$id."]", null);
		$this->cache->_set("users[tree]", null);
	}

	public function delete($id){
		// del user
		$this->dbObj->delete('items', "`id` = ".$id);
		$this->cache->_set("users[items][".$id."]", null);
	}
	
	public function deleteCategory($id){
		// del category and its childs
		// remove category info from users; set them 0 category
		$this->clearOfCategory($id);
	
		$childs = $this->getCategoryList($id);
		$ids = Array();
		foreach ($childs as $v) {
			$ids[] = $v['id'];
		}
			
		if (!empty($ids)) $this->dbObj->delete('cats', "`id` IN (".implode(',', $ids).")");
		$this->dbObj->delete('cats', "`id` = ".$id);
		$this->cache->_set("users[tree]", null);
	}

	public function login($entry){
		$result = null;
		$user = $this->get($entry['id']);
		if (!empty($user['id'])) {
			if ( crypt($entry['pass'], $user['pass']) === $user['pass'] ) {
				$_SESSION['user'] = $user;
				Main::log($user['login']." (".$user['email'].") logged in", true);
				$result = $user;
			}
		}
	
		return $result;
	}
	
	public static function logout(){
		$user = $_SESSION['user'];
		Main::log($user['login']." (".@$user['email'].") logged out", true);
		unset($_SESSION['user']);
		unset($_SESSION['user_redirect_link']);
		unset($_SESSION['settings']);
	}
	
	public function authUser($level_limit){
		// check user's access level
		$result = false;
		$user = Array();
	
		if (!empty($_SESSION['user']['id'])) {
			$user = $_SESSION['user'];
			$category = $this->getCategory($user['category']);
			$level = $category['level'];
				
			if (!empty($level)) {
				if ($level <= $level_limit) {
					$result = $user;
				}
			}
		}
			
		return $result;
	}
	
	
	private function prepare($entry, $id = null){
		$file_size_limit = 10240000;
		$GLOBALS['log'][] = $entry;
		
		if (!empty($entry['pass'])) {
			if ($entry['pass'] != "****************") {
				$entry['pass'] = $this->passHash($entry['pass']);
			} else unset($entry['pass']);

		} else $entry['error'][] = "<b>Password</b> is required";

		if (!$id) {
			if (!empty($entry['email'])) {
				$user = $this->get($entry['email']);
				if (!empty($user['id'])) $entry['error'][] = "Email <b>".$entry['email']."</b> already used";
			}
			if (!empty($entry['login'])) {
				$user = $this->get($entry['login']);
				if (!empty($user['id'])) $entry['error'][] = "Login <b>".$entry['login']."</b> already used";
			}
		}

		if (!empty($_FILES['image']['tmp_name'])) {
			// create files object
			$filesObj = $this->mainObj->load('files');
			
			$GLOBALS['log'][] = $_FILES;
			$image_params = Array('width' => 200, 'quality' => 85); 
			$image = $filesObj->uploadFile($_FILES['image'], "image", $file_size_limit, $this->classname."/images/", $image_params);
			$GLOBALS['log'][] = $image;
			
			if (empty($image['error'])) {
				$entry['image'] = $image['path'];
			} else {
				$entry['error'][] = "Image upload error: ".$image['error'][0];
			}
		}
		
		return $entry;
	}

	private function prepareCategory($id, $entry){
		if (empty($entry['parent_info']) && !empty($entry['parent'])) {
			$parent_info = $this->getCategory($entry['parent']);
			if ($entry['level'] < $parent_info['level']) $entry['level'] = $parent_info['level'];
		}
		$old_entry = $this->getCategory($id);
		if ($entry['level'] != $old_entry['level']) {
			// need to update childs if access level has been changed
			$childs = $this->getCategoryList($id);
			foreach ($childs as $k => $cat) {
				if ($entry['level'] > $cat['level']) {
					$cat['level'] = $entry['level'];
					$this->updateCategory($cat['id'], $cat);
				}
			}
	
		}
	
	
		return $entry;
	}
	

	private function passHash($pass){
		$cost = 10;
		$salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
		$salt = sprintf("$2a$%02d$", $cost) . $salt;
			
		$hash = crypt($pass, $salt);
		
		return $hash;
	}

	private function clearOfCategory($cat_id){
		// update list of items 
		$entry['category'] = 0;
		$result = $this->dbObj->update('items', $entry, "`category` = ".$cat_id);
		
	}
	
	private function parseItem($entry){
		if ($entry['category'] != 0 && empty($entry['category_info'])) {
			$entry['category_info'] = $this->getCategory($entry['category']);
			$entry['level'] = (!empty($entry['category_info']['level'])) ? $entry['category_info']['level'] : null;
		} elseif ($entry['category'] == 0) {
			// root user
			$entry['level'] = 1;
		}
		
		return $entry;
	}

}

?>