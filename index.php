<?php
	// main index
	class Index{
		
		public static function main(){
			return new self();
		}
		
		public function __construct(){
			$start_time = microtime(true);
			session_start();
			ob_start();	// output buffering

			$uri_parts = array();
			
			require_once ('config.php');
			// load kernel files
			require_once (KERNEL_C.'main.class.php');

			// fetch uri
			$uri_parts = Main::get_uri_parts();
			$clean_uri = implode('/', $uri_parts);

			if ($uri_parts[0] == "admin") {
				require_once(ROOT."admin/controllers/index.php");
				exit; 
			}	
			
			// create TemplateEngine object
			$tpl = new TemplateEngine();

			$mainObj = Main::getInstance();
			$mainObj->setTpl($tpl);
			//	route
			if ($clean_uri == "index.php") {
				$mainObj->redirect("/");
			} else $page = $mainObj->handlePublicURI($uri_parts);
			
			$tpl->assign('user', (!empty($_SESSION['user'])) ? $_SESSION['user'] : Array());
			$tpl->assign('root', URL_PREFIX);
			$tpl->assign('location', $mainObj->getUri());
			$tpl->assign('page', $page);
			
			//$GLOBALS['log'][] = "<var>mainObj->uri</var> = ". $mainObj->getUri();
			
			if (empty($page['template'])) $page['template'] = "_default";
			

			if (MULTILANG){
				// for Multilanguage sites. Parses text tags.
				// Run teg parser	
				require ROOT.'langparser.php';
				echo $page['page_fetched'];
			} else {
				// for Single Language sites. Just displays templates.
				$tpl->display('_header.html');
				$tpl->display($page['template'].".html");
				$tpl->display('_footer.html');
			}
			
			$end_time = microtime(true);
			$duration = $end_time - $start_time;

			$GLOBALS['log'][] = "Page generated in ".$duration." sec. ".(($GLOBALS['db_active']) ? "using DB" : "from Cache");
			
			
			
			/*
			require_once ('refactor.php');
			$Refactor = new Refactor();
			*/
			Main::print_debug();
			//Main::log($duration, false);
			//Main::Redirect();
		}
	}
	Index::main();
?>