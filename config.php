<?php
	if (!$this) die();
	// debug vars {
	$GLOBALS['db_active'] = 0;
	$GLOBALS['log'] = Array();
	// debug vars }
	if (empty($_SESSION['settings_time'])) $_SESSION['settings_time'] = 0;
	// update settings for a user every 10 min
	if (empty($_SESSION['settings']) || ((time() - $_SESSION['settings_time']) > 600)) {
		$_SESSION['settings'] = parse_ini_file("config.ini");
		$_SESSION['settings_time'] = time();
		
		$GLOBALS['log'][] = "Settings updated";
	}

	if (is_array($_SESSION['settings'])){
		foreach($_SESSION['settings'] as $name => $value) {
			if (!empty($value) || $value == '0') {
				define("$name", $value);
			}
		}
	}
	
	if (!defined("URL_PREFIX")) {
		// try to detect working directory
		$pwd = Array();
		preg_match_all("#www.*\/#", $_SERVER['SCRIPT_FILENAME'], $pwd);
		$pwd = substr($pwd[0][0], 3);
		define("URL_PREFIX", $pwd);
	}

	define ('ROOT', $_SERVER['DOCUMENT_ROOT'].URL_PREFIX);
	define('SMARTY_DIR', ROOT.'lib/smarty/libs/');
	define('KERNEL_C', ROOT.'kernel/');
	
	// Require libs
	require_once(SMARTY_DIR.'Smarty.class.php');
	require_once(ROOT.'/lib/phpfastcache/phpfastcache.php');
	
	define("MODULE_DIR", ROOT.'modules/');
	define('UPLOAD_DIR', ROOT.'uploads/');
	define('TEMPLATE_DIR', ROOT.'templates/');
	
	define("SUPERUSER", "root");
	define("SUPERPASS", sha1("123"));
	
	define("LOG", ROOT."log.log");
	
	// Cache config
	// create chache dir if it doesn't exist
	$cache_dir = "cache/";
	if (!file_exists($cache_dir)) mkdir($cache_dir, 0777, true);
	phpFastCache::setup("storage","files");
	phpFastCache::setup("path", ROOT.$cache_dir);
	
	// Smarty config
	class TemplateEngine extends Smarty {
		private $tpl_cache = "cache/smarty_tpl/";
		function __construct() {
			parent::__construct();
			$this->debugging = false;
			$this->error_reporting="E_NONE";
			$this->template_dir = ROOT.'templates/';
			// create chache dir if it doesn't exist
			if (!file_exists($this->tpl_cache)) mkdir($this->tpl_cache, 0777, true);
			$this->compile_dir = ROOT.$this->tpl_cache;
			$this->config_dir = ROOT.'configs/';
			$this->cache_dir = ROOT.'cache/';
			// don't forget!
			$this->caching = false;
			$this->assign('app_name', APPNAME);
		}

	}

?>