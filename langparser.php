<?php
	if (!defined('APPNAME')) die();
	
	//$start_time = microtime(false);
	
	// parse tags
	$lang_id;
	$langObj = $mainObj->load('lang', $mainObj);
	
	if (!empty($_GET['lang'])) {
		$lang_list = $langObj->getLangList();
		$lang_id = intval($_GET['lang']);
		if (array_key_exists($lang_id, $lang_list)) {
			$_SESSION['user']['lang_id'] = $lang_id;
		} 
	} 
	if (!empty($_SESSION['user']['lang_id'])) {
		$lang_id = $_SESSION['user']['lang_id'];
	} else $lang_id = DEFAULT_LANG;

	$page['page_fetched'] = '';
	
	$page['header_fetched'] = $tpl->fetch($tpl->template_dir[0].'_header.html');
	
	// main tpl for the page
	$tpl_path = '';
	if ($page['template'] != '_default' && $page['template'] != 'error'){
		$tpl_path = $page['template'].'.html';
	} else {
		$tpl_path = $tpl->template_dir[0].$page['template'].'.html';
	}	
	
	$page['template_fetched'] = $tpl->fetch($tpl_path);
	$page['footer_fetched'] = $tpl->fetch($tpl->template_dir[0].'_footer.html');

	// Merge templates
	$page['page_fetched'] = $page['header_fetched'].$page['template_fetched'].$page['footer_fetched'];
	// Parse tags
	$page['page_fetched'] = $langObj->parse($page['page_fetched'], $lang_id, $clean_uri);
	
	
	//$end_time = microtime(false);
	//$duration = $end_time - $start_time;
	//echo "Tags parsed in ".$duration." s";
	
