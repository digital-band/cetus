<?php
	// admin index
	class AdminIndex{
		public static function init(){
			return new self();
		}
		
		public function __construct(){
			require_once (KERNEL_C.'main.class.php');
			
			$tpl = new TemplateEngine();
			$tpl->template_dir = ROOT.'admin/views/';
			
			$mainObj = Main::getInstance();
			$mainObj->setTpl($tpl);

			$page = Array();

			$page['template'] = "index";
			$page['title'] = APPNAME;
			$page['text'] = "Here goes some statistics or whatever..";
			
			// routing
			$uri_parts = $mainObj->get_uri_parts();
			$page = $mainObj->handlePrivateURI($uri_parts);

			$module_list = $mainObj->modulesListExt();
			
			$tpl->assign('page', $page);
			
			// authorization
			if ($user = Main::authorize(MINLVL)) {
				$tpl->assign('user', $user);
				$tpl->display('_header.html');
				Main::print_debug();
				
				$tpl->display($page['template'].".html");
				$tpl->display('_footer.html');
			} else {
				$page['template'] = 'login';
				$page['title'] = "Login";
				$tpl->assign('page', $page);
				$tpl->display($page['template'].".html");
				
			}
		}		
	}
	
	if (defined('APPNAME'))  {
		AdminIndex::init();
	} else header('Location: /admin/');

?>