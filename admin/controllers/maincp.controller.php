<?php 

class MainCP extends CP{
	protected $obj;
	protected $mainObj;
	// set access levels
	protected $sec_levels = Array(
		"settings" => 5,
	);
	
	function __construct($mainObj, $moduleObj){
		// load basic features
		parent::__construct($mainObj, $moduleObj);		
	}
		
	public function action_main(){
		$this->mainObj->setPageProperty('text', "Main");
	}
	
	public function action_login(){
		if (!empty($_POST)) {
			Main::log("Trying to login");
			$entry['id'] = $_POST['id'];
			$entry['pass'] = $_POST['pass'];

			$user = $this->mainObj->login($entry);
			
			if (empty($user)) {
				$usersObj = $this->mainObj->load("users", $this->mainObj);
				$user = $usersObj->login($entry);
			}
			
			if (!empty($user) && Main::authorize(MINLVL)) {
				if (!empty($_SESSION['user_redirect_link'])) Main::log("Redirect to: ".$_SESSION['user_redirect_link']);
				$this->mainObj->redirect((!empty($_SESSION['user_redirect_link'])) ? $_SESSION['user_redirect_link'] : "admin/");
			} else {
				$this->mainObj->assign("error", "Login error. Try again.");
				$this->mainObj->assign("login_data", $entry);
				Main::log("Login error: ".$entry['id']." / ". $entry['pass']);
			}
		}
		
		$this->mainObj->setPageProperty('template', 'login');
		$this->mainObj->setPageProperty('title', "Login");
	}
	
	public function action_logout(){
		$user = $_SESSION['user'];
		if ($user['login'] == SUPERUSER) {
			$this->mainObj->logout();
		} else {
			$usersObj = $this->mainObj->load("users", $this->mainObj);
			$usersObj->logout();
		}
		
		$this->mainObj->redirect("admin/");
	}
	
	public function action_settings(){
		if (!empty($_POST)){
			$this->obj->setSettings($_POST);
			$this->mainObj->redirect();
		} else {
			$settings = $this->obj->getSettings();
			$langObj = $this->mainObj->load('lang', $this->mainObj);
			$lang_list = $langObj->getLangList();

			$this->mainObj->setPageProperty('title', "Settings");
			$this->mainObj->setPageProperty('template', 'settings');
			$this->mainObj->assign('settings', $settings);
			$this->mainObj->assign('lang_list', $lang_list);
		}

	}
	
	public function action_poke(){
		echo "Poke!";
	}

	
}





?>