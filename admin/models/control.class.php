<?php 
class Control extends Module{
	protected $dbObj;
	protected $classname = "control";
	
	private $ini_file = "config.ini";
	private $iniObj;
	
	public function __construct($ini_file = null){
		parent::__construct();
		
		$this->iniObj = Main::load("ini");
		if ($ini_file) {
			// you can load this class using different config file
			$this->ini_file = $ini_file;
		}
	
	}
	
	public function getSettings(){
		$settings = Array();
		
		try {
			$settings = parse_ini_file($this->ini_file);
		} catch (Exception $e) {
			echo "Exception: cannot load ".$this->ini_file."<br>".$e;
		}
		
		return $settings;
	}
	
	public function setSettings($entry){
		$entry = $this->validateSettings($entry);
		$this->iniObj->setList($this->ini_file, $entry);
		$_SESSION['settings'] = parse_ini_file($this->ini_file);
	}
	
	public function setSettingsValue($entry){
	
	}
	
	public function validateSettings($entry){
		if ($entry['DB_PASS'] == "default") {
			unset($entry['DB_PASS']);
		}
		
		return $entry;
	}
	
}