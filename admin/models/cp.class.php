<?php
/* This class implements common methods for modules control panels
 * cp class of a module must extend this class 
 * 
 */ 
class CP {
	// instance of controller 
	private $controllerObj = null;
	// requested action
	private $action = null;
	// user access level
	private $u_level = null;
	// module parameters (config.php)
	private $params = null;
	private $uri;
	
	// controller access (security) levels
	protected $sec_levels = Array();
	// module model object
	protected $obj;
	protected $mainObj;
	// requested in controller id
	protected $id = null;
	
	protected function __construct(Main $mainObj, $moduleObj = null){
		$this->obj = $moduleObj;
		$this->mainObj = $mainObj;
		$this->uri = $this->mainObj->getUri();
		if ($this->obj) $this->params = $this->obj->getParams();

		$this->run();
	}
	
	// getters {
	public function getSecLevels(){
		return $this->sec_levels;
	}
	// getters }
	
	// setters {
	public function setParams($params = null){
		$this->params = $this->obj->getParams($params);
	}
	// setters }
		
	private function run(){
		set_error_handler(array($this, "ActionNotFoundException"));
		
		// access checking
		if ($this->mainObj->getUriPart(1)) {
			if ($this->params) {
				// get a level and authoricate
				if (!empty($this->params['a_level'])) {
					$user = Main::authorize($this->params['a_level']);

					if (!$user) {
						Main::log("Trying to access being not authorized: ".$this->uri);
						$_SESSION['user_redirect_link'] = preg_replace("#^\/#", "", $this->uri);
						$this->mainObj->redirect("admin/");
					}
				}
			}			
		}
		
		$this->orderTable();

		// detect action
		if ($this->obj->getModulename()) {
			// if any module loaded
			$this->action = ($this->mainObj->getUriParts(2)) ? $this->mainObj->getUriParts(2) : "main";
			$title = $this->obj->getModulename()." Control Panel";
			$this->mainObj->setPageProperty('title', $title);
		} else {
			// for common actions
			$this->action = ($this->mainObj->getUriParts(1)) ? $this->mainObj->getUriParts(1) : "main";
		}
		
		// get item id
		if ($this->mainObj->getUriParts(3)) {
			if (is_numeric($this->mainObj->getUriParts(3))) {
				$this->id = intval($this->mainObj->getUriParts(3));
			} else {
				// error
				$this->mainObj->setPageProperty('template', "error");
				$this->mainObj->setPageProperty('text', "Invalid id: ".$this->mainObj->getUriParts(3));
				$GLOBALS['debug_info'][] 	= "Invalid id: ".$this->mainObj->getUriParts(3);
				
				return $this->mainObj->getPage();
			}
		}
		
		// check if the controller has the action
		if (method_exists($this, "action_".$this->action)) {
			$this->controllerObj = $this;
		} else if ($this->obj->getModulename()) {
			// error
			$this->mainObj->setPageProperty('template', "error");
			$this->mainObj->setPageProperty('text', "Invalid action: ".$this->action);
			$GLOBALS['debug_info'][] = "Invalid action requested: ".$this->obj->getClassname()." -> ".$this->action;
			
			return $this->mainObj->getPage();
		}
		
		// get user access level
		if (!empty($_SESSION['user'])) {
			$this->u_level = $_SESSION['user']['level'];
			$GLOBALS['debug_info'][] = "Your access level: ".$this->u_level;
		}
		
		if ($this->controllerObj) {
			$a_level = null;
			if (!empty($this->sec_levels[$this->action])) $a_level = $this->sec_levels[$this->action];
			
			$GLOBALS['debug_info'][] = "Action access level: ".$a_level;
			
			if ($a_level == null || Main::authorize($a_level)) {
				try {
					call_user_func(array($this->controllerObj, "action_".$this->action));
					//$GLOBALS['debug_info'][] = Main::Print_r($this->entry);
					if (!empty($this->id) && empty($this->entry['id'])) {
						$GLOBALS['debug_info'][] = "Invalid <var>id</var> requested: ".$this->id;
						$this->mainObj->setPageProperty('template', "error");
						$this->mainObj->setPageProperty('text', "Invalid item requested: ".$this->id);
					}
				} catch (ErrorException $e) {
					$title = 'Invalid Action: '.$this->action;
					$this->mainObj->setPageProperty('title', $title);
					$this->mainObj->setPageProperty('text', $title);
					$GLOBALS['debug_info'][] = $e;
					Main::log($e);
					
				}
			} else {
				$GLOBALS['debug_info'][] = "Permission denied. Min level: ".$a_level;
				Main::log("Permission denied.");
				$this->mainObj->setPageProperty('template', "error");
				$this->mainObj->setPageProperty('text', "Permission denied. Minimal level required: ".$a_level);
			}
		}
	
		// load top menu
		if (method_exists($this->obj, 'GetTopMenu')) {
			$tree = $this->obj->getTopMenu();
			$this->mainObj->assign('tree', $tree);
		}
		
		// set template vars
		if ($this->obj->getClassname() != "control") $this->mainObj->assign('module', $this->obj->getClassname());
		if (!empty($this->sec_levels)) $this->mainObj->assign('a_levels', $this->sec_levels);
		
		// load access levels
		if (!empty($this->u_level) && !empty($this->sec_levels)) {
			$locked = Array();
			foreach ($this->sec_levels as $action => $level) {
				$locked[$action] = ($this->u_level <= $level) ? 0 : 1;
			}
			//$GLOBALS['log'][] = $locked;
			$this->mainObj->append('locked', $locked, true);
		}
		
		// request available modules for the user 
		if (!empty($_SESSION['user']['level'])) {
			$module_list = $this->mainObj->getModulesExt($_SESSION['user']['level']);
			$this->mainObj->assign(Array(
					'root' => "/admin/",
					'module_list' => $module_list,
			));
		}
	}
	
	protected function orderTable() {
		// table operations
		$order = 0;
		$direction = 0;
	
		if (!empty($_GET['order']))		$order = $_GET['order'];
		if (!empty($_GET['direction']))	$direction = $_GET['direction'];
	
		$this->order = $order;
		$this->direction = $direction;
		$direction = ($this->direction == 0) ? 1 : 0;
		$this->mainObj->assign('direction', $direction);
	}
	
	private function actionNotFoundException($errno, $errstr, $errfile, $errline, array $errcontext) {
		if (0 === error_reporting()) {
			return false;
		}
		throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
	}
	
}

?>