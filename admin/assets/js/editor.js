
 2    $().ready(function() {
 3        var opts = {
 4            lang         : 'en',   // set your language
 5            styleWithCSS : true,
 6            height       : 400,
 7            toolbar      : 'maxi'
 8        };
 9        // create editor
10        $('#html-editor').elrte(opts);
11
12        // or this way
13        // var editor = new elRTE(document.getElementById('our-element'), opts);
14    });
